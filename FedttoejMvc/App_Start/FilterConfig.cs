﻿using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Linq;

namespace FedttoejMvc
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new GlobalExceptionFilter());
        }


    }



    public class GlobalExceptionFilter : IExceptionFilter
    {
        public GlobalExceptionFilter()
        {
        }

        public void OnException(ExceptionContext context)
        {
            Task.Run(async () =>
            {
                using (var client = new CrashlogWebClient())
                {
                    await client.SendException(context.Exception, GetExtras(context.HttpContext));
                }
            });
        }

        public List<CrashlogWebClient.ExtraData> GetExtras(HttpContextBase httpContext)
        {
            var extras = new List<CrashlogWebClient.ExtraData>();

            var httpUrlExtras = new CrashlogWebClient.ExtraData
            {
                Title = "HTTP_URL",
                KeyValues = new List<KeyValuePair<string, string>>()
            };
            if (httpContext.Request.Url != null)
            {
                httpUrlExtras.KeyValues.Add(new KeyValuePair<string, string>("Scheme", httpContext.Request.Url.Scheme));
                httpUrlExtras.KeyValues.Add(new KeyValuePair<string, string>("Host", httpContext.Request.Url.Host));
            }
            httpUrlExtras.KeyValues.Add(new KeyValuePair<string, string>("Path", httpContext.Request.RawUrl));
            httpUrlExtras.KeyValues.Add(new KeyValuePair<string, string>("QueryString", httpContext.Request.QueryString.ToString()));
            httpUrlExtras.KeyValues.Add(new KeyValuePair<string, string>("Method", httpContext.Request.RequestType));

            extras.Add(httpUrlExtras);

            var httpHeadersExtra = new CrashlogWebClient.ExtraData
            {
                Title = "HTTP_HEADERS",
                KeyValues = new List<KeyValuePair<string, string>>()
            };
           
            httpHeadersExtra.KeyValues.AddRange(httpContext.Request.Headers.AllKeys.Select(x => new KeyValuePair<string, string>(x, httpContext.Request.Headers[x])).Where(x => x.Key != "Authorization"));
            
            extras.Add(httpHeadersExtra);
            return extras;
        }
    }

    public class CrashlogWebClient : HttpClient
    {
        private const string API_KEY = "nsN/zEs58JUPamsN3GjdkYYnLSd9uiJp/kSN4hEilbYkSsF8+sal9FaUE2VrBXlZXbbucihPMFndnJTpwawj/DLfjvk7hZwsUcIAtOI6/rIiHjAVZrHkJcCUE4Lg2aGR8MaqTARQoRapaTa87k9z9AfVXoBsgrhqFNaHTCSazzgyMjceB0+xjwn0k/5TSu9+H1YUJ9AznNvKar+cQdATpn5WwSO8vODBtE65i9hi/RI1BPcf8AjNbgoDUjSfb2oYYfNSqcoxGUP1A9lwGCrrPKJuQB2afl81ginjudlCKtOvpV2eLaiMtANs4lKRg8sJkjDcm26fYmXlhxqNY5U5zo9Pb2iHoXx1TeKqEdlv69WpmO1W/xvC632XyoaNnoDndDURJNf0T6dBYw5x/gthPM0ovIiOYeft8GLmoByfm7x8MjKPf5RZ2XvhHWAh12UFy+tkT+C8bH9mYtCSWTZ9/ZUVsknJi9SXEkLEuSUCmOW7H9OhI3TtTn372aoXNGF5rZx9JgZGop9crMXL2dUyTKVl8THmaOl9E9GAJUcXPSCtCnF8RCxIGTbk9ZkYjpWtJM7ihBPBkpWBjRFufuvpRrzi7TUZRnHHomhp+mPFxCmmuq2Vo50hmhCeYoSAAmy53rBxgH0lNPRwJU5HMTxrMyM38B5jygDJbxFKr1MVlsU=";
        private const string BASE_URL = "http://tasks.andersfa.dk/api/";

        public CrashlogWebClient()
        {
            DefaultRequestHeaders.Accept.Clear();
            DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("ApiKey", API_KEY);
        }

        public async Task SendException(Exception exception, List<ExtraData> extras)
        {
            try
            {
                var stacktracebuilder = new StringBuilder();
                Exception currentException = exception;
                while (currentException != null)
                {
                    stacktracebuilder.AppendFormat("Message:\n{0}\n", currentException.Message);
                    stacktracebuilder.AppendFormat("StackTrace:\n{0}\n\n", currentException.StackTrace);
                    currentException = currentException.InnerException;
                    if (currentException != null)
                    {
                        stacktracebuilder.AppendLine("-----------------------------------------------------");
                    }
                }

                var payload = JsonConvert.SerializeObject(new
                {
                    message = exception.Message,
                    stacktrace = stacktracebuilder.ToString(),
                    type = exception.GetType().FullName,
                    severity = Severity.Normal,
                    extras = extras
                });

                var url = string.Format("{0}error", BASE_URL);
                var response = await PutAsync(url, new StringContent(payload, Encoding.UTF8, "application/json"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public enum Severity
        {
            Normal = 1,
            Critical = 10
        }

        public class ExtraData
        {
            public string Title { get; set; }
            public List<KeyValuePair<string, string>> KeyValues { get; set; }
        }

    }




}

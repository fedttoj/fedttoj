﻿using FedttoejMvc.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Models.Data
{
    public static class VideoMethods
    {
        private static List<VideoViewModel> AllVideos;

        public static List<VideoViewModel> GetAllVideos(bool force = false)
        {
            if (AllVideos == null || force)
            {
                using (var context = new TablesDataContext())
                {
                    AllVideos = new List<VideoViewModel>();
                    foreach (var video in context.Videos.OrderByDescending(x => x.CreatedDate))
                    {
                        AllVideos.Add(VideoViewModel.ToViewModel(video));
                    }
                }
            }
            return AllVideos;
        }

        public static VideoViewModel GetVideoById(int id)
        {
            using (var context = new TablesDataContext())
            {

                var video = context.Videos.FirstOrDefault(x => x.Id == id);
                if (video == null)
                    return new VideoViewModel();

                return VideoViewModel.ToViewModel(video);
            }
        }

        public static VideoViewModel GetVideoByUrl(string url)
        {
            var video = GetAllVideos().FirstOrDefault(x => x.Url == url);
            if (video == null)
                return null;

            return video;

        }

        public static Video SaveVideo(VideoViewModel viewModel)
        {
            using (var context = new TablesDataContext())
            {
                var isNew = true;
                var video = context.Videos.FirstOrDefault(x => x.Id == viewModel.Id);
                if (video == null)
                    video = new Video { CreatedDate = DateTime.Now };
                else
                    isNew = false;

                video.Id = viewModel.Id;
                video.Title = viewModel.Title;
                video.Url = viewModel.Url;
                video.Body = viewModel.Body;
                video.YoutubeUrl = viewModel.YoutubeUrl;
                video.PublishDateFriendly = viewModel.PublishDateFriendly;
                video.PublishDateRobot = viewModel.PublishDateRobot;
                video.MetaDescription = viewModel.MetaDescription;
                video.MetaKeywords = viewModel.MetaKeywords;
                video.PictureName = viewModel.PictureName;

                if (isNew)
                    context.Videos.InsertOnSubmit(video);

                context.SubmitChanges();
                GetAllVideos(true);
                return video;
            }
        }
    }
}
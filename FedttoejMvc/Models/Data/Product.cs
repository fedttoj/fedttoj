﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using FedttoejMvc.Models;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;

/// <summary>
/// Summary description for Product
/// </summary>
public class Product
{
    public int Id { get; set; }
    public int MainCategoryId { get; set; }
    public string MainCategoryName { get; set; }
    public int CategoryId { get; set; }
    public string CategoryName { get; set; }
    public int CompanyId { get; set; }
    public string CompanyName { get; set; }
    public string Make { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Link { get; set; }
    public string Picture { get; set; }
    public double Price { get; set; }
    public int Status { get; set; }
    public int MakeId { get; set; }
    public Utilities.Area TheArea { get; set; }

    private static DateTime LastRenewed { get; set; }
    private static IndexSearcher TheSearcher { get; set; }
    
    public static Product GetProduct(int id, string indexPath)
    {
     
        Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
       var mainQuery = new BooleanQuery();

        
        var query = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "Id", analyzer).Parse(id.ToString());
        mainQuery.Add(query, BooleanClause.Occur.MUST);

        if (TheSearcher == null || LastRenewed == null || LastRenewed < DateTime.Now.AddDays(-1))
        {
            if (TheSearcher != null)
                TheSearcher.Close();
            Directory index = FSDirectory.Open(new DirectoryInfo(indexPath));
            var idxReader = IndexReader.Open(index, true);
            TheSearcher = new IndexSearcher(idxReader);
            LastRenewed = DateTime.Now;
        }

        var theCollector = TopFieldCollector.create(new Sort(), 1, true, true, true, false);

        TheSearcher.Search(mainQuery, theCollector);
        var hits = theCollector.TopDocs().scoreDocs;
        Product item = null;
        if (hits.Length > 0)
        {
            var docId = hits[0].doc;

            var d = TheSearcher.Doc(docId);
            item = new Product()
                {
                    Id = int.Parse(d.Get("Id")),
                    CategoryId = int.Parse(d.Get("CategoryId")),
                    CategoryName = d.Get("CategoryName"),
                    Description = d.Get("Description"),
                    CompanyId = int.Parse(d.Get("CompanyId")),
                    Status = int.Parse(d.Get("Status")),
                    CompanyName = d.Get("CompanyName"),
                    Link = d.Get("Link"),
                    MainCategoryId = int.Parse(d.Get("MainCategoryId")),
                    MainCategoryName = d.Get("MainCategoryName"),
                    Picture = d.Get("Picture"),
                    Title = d.Get("Title").Replace("?", ""),
                    TheArea = (Utilities.Area) int.Parse(d.Get("Area")),
                    Price = double.Parse(d.Get("Price")),
                    MakeId = int.Parse(d.Get("MakeId")),
                    Make = !string.IsNullOrWhiteSpace(d.Get("Make")) ? d.Get("Make").Replace("qwerty", ":").Replace("ertyu", "´") : ""

                };

            
        }

       
        analyzer.Close();
       
        return item;
    }

    // Lucene search
    public static ProductSearchQuery SearchLucene(ProductSearchQuery productQuery)
    {
        var result = new List<Product>();
        var searchString =
            productQuery.SearchString.Trim().Replace("-", " ").Replace("+", " ").Replace("*", " ").Replace("?", " ").
                Replace("(", " ").Replace(")", " ").Replace("!", " ").Replace("&", " ").Replace("^", "").Replace("'", " ").Replace(":", " ").Replace("´", "").Replace("\"", "").Replace("[", "").Replace("]", "");

        //if (!string.IsNullOrWhiteSpace(productQuery.Make))
        //    productQuery.Make = productQuery.Make.Trim().Replace("-", "\\-").Replace("+", "\\+").Replace("*", "\\*").Replace("?", "\\?").
        //        Replace("(", " ").Replace(")", " ").Replace("!", "\\!").Replace("^", "\\^").Replace("'", "trewq").Replace(":", "qwerty").Replace("´", "ertyu");

        const int maxHits = 600000;
        if (productQuery.MaxHits == 0)
            productQuery.MaxHits = maxHits;

        var startIndex = (productQuery.PageNumber - 1) * productQuery.HitsPerPage;

        if (searchString.Length > 1 && searchString.Substring(searchString.Length - 1) != " ")
            searchString += "*";

        Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
       
        var mainQuery = new BooleanQuery();

        if (searchString.Length > 0)
        {
            searchString = searchString.Replace("/", " ").Replace("*", " ");
            var sStringArr = searchString.Trim().Split(' ');
            foreach (string str in sStringArr)
            {
                if (str.Trim() != "" && str.Trim().ToLower() != "and" && str.Trim().ToLower() != "not" && str.Trim().ToLower() != "or" && str.Trim().ToLower() != "of" && str.Trim().ToLower() != "a")
                {
                    
                    var query = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "SearchBody", analyzer).Parse(str);
                    if (productQuery.SpecificSearch)
                        mainQuery.Add(query, BooleanClause.Occur.MUST);
                    else
                        mainQuery.Add(query, BooleanClause.Occur.SHOULD);
                }
            }

        }

        var statusQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "Status", analyzer).Parse("1");
        mainQuery.Add(statusQuery, BooleanClause.Occur.MUST);

        if (productQuery.MainCategoryId > 0)
        {
            var mainCatQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "MainCategoryId", analyzer).Parse(productQuery.MainCategoryId.ToString());
            mainQuery.Add(mainCatQuery, BooleanClause.Occur.MUST);
        }
        else if (productQuery.CategoryId > 0)
        {
            var catQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "CategoryId", analyzer).Parse(productQuery.CategoryId.ToString());
            mainQuery.Add(catQuery, BooleanClause.Occur.MUST);
        }

        if (productQuery.MakeId > 0)
        {
            var makeQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "MakeId", analyzer).Parse(productQuery.MakeId.ToString());
            mainQuery.Add(makeQuery, BooleanClause.Occur.MUST);
        }

        if (productQuery.PriceTo > 0)
        {
            var priceQuery = NumericRangeQuery.NewIntRange("PriceSort", productQuery.PriceFrom * 100, productQuery.PriceTo * 100, true, true);
            mainQuery.Add(priceQuery, BooleanClause.Occur.MUST);
        }

        //if (!string.IsNullOrEmpty(productQuery.Make))
        //{
        //    var makeQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "MakeId", analyzer).Parse(productQuery.Make.Replace(":", "qwerty"));
        //    mainQuery.Add(makeQuery, BooleanClause.Occur.MUST);
        //}

        if (productQuery.ExcludeMisc)
        {
            var areaQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "Area", analyzer).Parse("4");
            mainQuery.Add(areaQuery, BooleanClause.Occur.MUST_NOT);
        }

         if (productQuery.TheArea != Utilities.Area.All)
        {
            var areaQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "Area", analyzer).Parse(((int)productQuery.TheArea).ToString());
            mainQuery.Add(areaQuery, BooleanClause.Occur.MUST);
        }

        if (TheSearcher == null || LastRenewed == null || LastRenewed < DateTime.Now.AddDays(-1))
        {
            if (TheSearcher != null)
                TheSearcher.Close();
            Directory index = FSDirectory.Open(new DirectoryInfo(productQuery.IndexPath));
            var idxReader = IndexReader.Open(index, true);
            TheSearcher = new IndexSearcher(idxReader);
            LastRenewed = DateTime.Now;
        }
        TopFieldCollector theCollector;

        if (productQuery.SortBy == SortType.Id)
            theCollector = TopFieldCollector.create(new Sort(new SortField("IdInt", SortField.INT, true)), productQuery.MaxHits, true, false, false, false);
        if (productQuery.SortBy == SortType.IdAsc)
            theCollector = TopFieldCollector.create(new Sort(new SortField("IdInt", SortField.INT, false)), productQuery.MaxHits, true, false, false, false);
        else if (productQuery.SortBy == SortType.PriceAsc)
            theCollector = TopFieldCollector.create(new Sort(new SortField("PriceSort", SortField.INT, false)), productQuery.MaxHits, true, false, false, false);
        else if (productQuery.SortBy == SortType.Price)
            theCollector = TopFieldCollector.create(new Sort(new SortField("PriceSort", SortField.INT, true)), productQuery.MaxHits, true, false, false, false);
        else
            theCollector = TopFieldCollector.create(new Sort(), productQuery.MaxHits, true, true, true, false);

        TheSearcher.Search(mainQuery, theCollector);
        var hits = theCollector.TopDocs().scoreDocs;

        productQuery.ReturnedRows = hits.Length;

        for (var i = startIndex; i < (startIndex + productQuery.HitsPerPage); ++i)
        {
            if (i >= hits.Length) continue;
            var docId = hits[i].doc;

            var d = TheSearcher.Doc(docId);
            var item = new Product()
                           {
                               Id = int.Parse(d.Get("Id")),
                               CategoryId = int.Parse(d.Get("CategoryId")),
                               CategoryName = d.Get("CategoryName"),
                               Description = d.Get("Description"),
                               CompanyId = int.Parse(d.Get("CompanyId")),
                               Status = int.Parse(d.Get("Status")),
                               CompanyName = d.Get("CompanyName"),
                               Link = d.Get("Link"),
                               MainCategoryId = int.Parse(d.Get("MainCategoryId")),
                               MainCategoryName = d.Get("MainCategoryName"),
                               Picture = d.Get("Picture"),
                               Title = d.Get("Title").Replace("?", ""),
                               TheArea = (Utilities.Area)int.Parse(d.Get("Area")),
                               MakeId = int.Parse(d.Get("MakeId")),
                               Make = !string.IsNullOrWhiteSpace(d.Get("Make")) ? d.Get("Make").Replace("qwerty", ":").Replace("ertyu", "´") : ""
                              
                           };
            var price = d.Get("Price");
            double priceDouble = 0;
            double.TryParse(price, out priceDouble);
            item.Price = priceDouble;

            result.Add(item);
        }
        productQuery.Result = result;
        analyzer.Close();
        return productQuery;
    }

    public static void DeleteOld(int companyId)
    {

        SqlCommand command = null;
        SqlDataReader reader = null;
        SqlConnection connection = null;
        try
        {
            connection = DBHandler.GetConnection();
            command = new SqlCommand("FedtDeleteOldProducts", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.Add("@CompanyId", SqlDbType.Int).Value = companyId;
            command.ExecuteScalar();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
        finally
        {
            if (command != null)
                command.Dispose();
            if (reader != null)
                reader.Dispose();
        }
    }


}

public class ProductSearchQuery
{
    public string IndexPath { get; set; }
    public int PageNumber { get; set; }
    public int HitsPerPage { get; set; }
    public SortType SortBy { get; set; }
    public bool InvertedSort { get; set; }
    public int MakeId { get; set; }
    public int MainCategoryId { get; set; }
    public int CategoryId { get; set; }
    public string SearchString { get; set; }
    public Utilities.Area TheArea { get; set; }
    public List<Facet> MakeFacets { get; set; }
    public SortedList<string, int> MainCategoryFacets;
    public SortedList<string, int> CategoryFacets;
    public int ReturnedRows { get; set; }
    public int MaxHits { get; set; }
    public List<Product> Result { get; set; }
    public bool SpecificSearch { get; set; }
    public bool ExcludeMisc { get; set; }
    public int PriceFrom { get; set; }
    public int PriceTo { get; set; }

    public ProductSearchQuery()
    {
        SpecificSearch = true;
        ExcludeMisc = false;
    }
        

}

public class Facet
{
    public string Name { get; set; }
    public int Value { get; set; }
}

public enum SortType
{
    Id = 0,
    IdAsc = 1,
    PriceAsc = 2,
    Price = 3
}

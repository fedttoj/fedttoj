﻿using FedttoejMvc.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Models.Data
{
    public static class ArticleMethods
    {
        private static List<ArticleViewModel> AllArticles;

        public static List<ArticleViewModel> GetAllArticles(bool force = false)
        {
            if (AllArticles == null || force)
            {
                using (var context = new TablesDataContext())
                {
                    AllArticles = new List<ArticleViewModel>();
                    foreach (var article in context.Articles.OrderByDescending(x => x.CreatedDate))
                    {
                        AllArticles.Add(ArticleViewModel.ToViewModel(article));
                    }
                }
            }
            return AllArticles;
        }

        public static ArticleViewModel GetArticleById(int id)
        {
            using (var context = new TablesDataContext())
            {

                var article = context.Articles.FirstOrDefault(x => x.Id == id);
                if (article == null)
                    return new ArticleViewModel();

                return ArticleViewModel.ToViewModel(article);
            }
        }

        public static ArticleViewModel GetArticleByUrl(string url)
        {
            var article = GetAllArticles().FirstOrDefault(x => x.Url == url);
            if (article == null)
                return null;

            return article;

        }

        public static Article SaveArticle(ArticleViewModel viewModel)
        {
            using (var context = new TablesDataContext())
            {
                var isNew = true;
                var article = context.Articles.FirstOrDefault(x => x.Id == viewModel.Id);
                if (article == null)
                    article = new Article { CreatedDate = DateTime.Now };
                else
                    isNew = false;

                article.Id = viewModel.Id;
                article.Title = viewModel.Title;
                article.Url = viewModel.Url;
                article.Body = viewModel.Body;
                article.BodyMobile = viewModel.BodyMobile;
                article.PublishDateFriendly = viewModel.PublishDateFriendly;
                article.PublishDateRobot = viewModel.PublishDateRobot;
                article.MetaDescription = viewModel.MetaDescription;
                article.MetaKeywords = viewModel.MetaKeywords;
                article.PictureName = viewModel.PictureName;

                if (isNew)
                    context.Articles.InsertOnSubmit(article);

                context.SubmitChanges();
                GetAllArticles(true);
                return article;
            }
        }
    }
}
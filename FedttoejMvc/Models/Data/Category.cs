﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Models.ViewModels;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Index;
using Lucene.Net.QueryParsers;
using Lucene.Net.Search;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;
using System.Web;
using System.Web.Caching;

/// <summary>
/// Summary description for Category
/// </summary>
/// 
[Serializable]
public class Category
{
    public int CategoryId { get; set; }
    public int MainCategoryId { get; set; }
    public string MainCategoryName { get; set; }
    public string CategoryName { get; set; }
    private static IndexSearcher TheSearcher { get; set; }
    private static DateTime LastRenewed { get; set; }

    public static CategorySearchQuery SearchLucene(CategorySearchQuery galleryQuery, bool overrideCache = false)
    {
        var cacheName = "area" + (int)galleryQuery.TheArea + "main" + galleryQuery.MainCategoryId + "cat" + galleryQuery.CategoryId;

        System.Web.HttpContext httpContext = HttpContext.Current;
        var url = httpContext.Request.Url.AbsoluteUri;

        
        CategorySearchQuery cacheResult = null;      

        cacheResult = (CategorySearchQuery)HttpContext.Current.Cache[cacheName];


        if (overrideCache)
            cacheResult = null;
        if (cacheResult != null)
            return cacheResult;

        var searchString =
            galleryQuery.SearchString.Trim().Replace("-", " ").Replace("+", " ").Replace("*", " ").Replace("?", " ").
                Replace("(", " ").Replace(")", " ").Replace("!", " ").Replace("&", " ").Replace("^", "");

       
        const int maxHits = 1000000;
        
        if (searchString.Length > 1 && searchString.Substring(searchString.Length - 1) != " ")
            searchString += "*";

        Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
        
        var mainQuery = new BooleanQuery();

        if (searchString.Length > 0)
        {
            var sStringArr = searchString.Trim().Split(' ');
            foreach (string str in sStringArr)
            {
                if (str.Trim() != "")
                {
                    var query = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "SearchBody", analyzer).Parse(str);
                    mainQuery.Add(query, BooleanClause.Occur.MUST);
                }
            }

        }
        
        if (galleryQuery.MainCategoryId > 0)
        {
            var mainCatQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "MainCategoryId", analyzer).Parse(galleryQuery.MainCategoryId.ToString());
            mainQuery.Add(mainCatQuery, BooleanClause.Occur.MUST);
        }
        else if (galleryQuery.CategoryId > 0 && galleryQuery.TheArea != Utilities.Area.Diverse)
        {
            var catQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "CategoryId", analyzer).Parse(galleryQuery.CategoryId.ToString());
            mainQuery.Add(catQuery, BooleanClause.Occur.MUST);
        }

        //if (galleryQuery.MakeId > 0)
        //{
        //    var makeQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "MakeId", analyzer).Parse(galleryQuery.MakeId.ToString());
        //    mainQuery.Add(makeQuery, BooleanClause.Occur.MUST);
        //}

        var areaQuery = new QueryParser(Lucene.Net.Util.Version.LUCENE_29, "Area", analyzer).Parse(((int)galleryQuery.TheArea).ToString());
        mainQuery.Add(areaQuery, BooleanClause.Occur.MUST);

        if (TheSearcher == null || LastRenewed == null || LastRenewed < DateTime.Now.AddDays(-1))
        {
            if (TheSearcher != null)
                TheSearcher.Close();
            Directory index = FSDirectory.Open(new DirectoryInfo(galleryQuery.IndexPath));
            var idxReader = IndexReader.Open(index, true);
            TheSearcher = new IndexSearcher(idxReader);
            LastRenewed = DateTime.Now;
        }

        TopFieldCollector theCollector;
        theCollector = TopFieldCollector.create(new Sort(), maxHits, true, true, true, false);

        TheSearcher.Search(mainQuery, theCollector);
        var hits = theCollector.TopDocs().scoreDocs;
        var categoryResult = new List<Category>();
        var makeResult = new List<MakeCountViewModel>();
        var categoryStr = "";
        foreach (var scoreDoc in hits)
        {
            var docId = scoreDoc.doc;

            var d = TheSearcher.Doc(docId);
            
            var catItem = new Category()
            {
                CategoryId = int.Parse(d.Get("CategoryId")),
                CategoryName = d.Get("CategoryName"),
                MainCategoryId = int.Parse(d.Get("MainCategoryId")),
                MainCategoryName = d.Get("MainCategoryName")

            };

            if (!categoryStr.Contains(catItem.CategoryId + ":t."))
            {
                categoryResult.Add(catItem);
                categoryStr += catItem.CategoryId + ":t.";
            }
            if (galleryQuery.CategoryId == 0 || (galleryQuery.CategoryId > 0 && galleryQuery.CategoryId == catItem.CategoryId))
            {
                var makeStr = d.Get("Make");
                var makeId = int.Parse(d.Get("MakeId"));

                var make = makeResult.SingleOrDefault(x => x.Make.Id == makeId);
                if (make == null)
                {
                    make = new MakeCountViewModel {Count = 1, Make = new FedtMake {Id = makeId, MakeName = makeStr}};
                    makeResult.Add(make);
                }
                else
                {
                    make.Count++;
                }
                
            }
        }

        makeResult = makeResult.OrderByDescending(x => x.Count).ToList();

        galleryQuery.CategoryResult = categoryResult;
        galleryQuery.MakeResult = makeResult;
        analyzer.Close();

        HttpContext.Current.Cache.Insert(cacheName, galleryQuery, null, DateTime.Now.AddHours(30), Cache.NoSlidingExpiration);
      
        return galleryQuery;
    }


    public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
    {
        using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            binaryFormatter.Serialize(stream, objectToWrite);
        }
    }

  public static T ReadFromBinaryFile<T>(string filePath)
  {
        using (Stream stream = File.Open(filePath, FileMode.Open))
        {
            var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            return (T)binaryFormatter.Deserialize(stream);
        }
    }
    

}

[Serializable]
public class CategorySearchQuery
{
    public string IndexPath { get; set; }
    //public int MakeId { get; set; }
    public int MainCategoryId { get; set; }
    public int CategoryId { get; set; }
    public string SearchString { get; set; }
    public Utilities.Area TheArea { get; set; }
    public List<Category> CategoryResult { get; set; }
    public List<MakeCountViewModel> MakeResult { get; set; }
}
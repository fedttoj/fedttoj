﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for DBHandler
/// </summary>
public class DBHandler
{
    private static String connectionString = "";

    public DBHandler()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private static String ConnectionString
    {
        get
        {
            if (connectionString == "")
                connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["rasmu12_VolvoConnectionString"].ConnectionString;
            return connectionString;
        }
    }

    public static SqlConnection GetConnection()
    {
        SqlConnection connection = null;
        try
        {
            if (ConnectionString != "")
            {
                connection = new SqlConnection(ConnectionString);
                connection.Open();
            }
            else
            {
                throw new Exception("Ingen connectionstring");
            }
        }
        catch (SqlException ex)
        {
            throw new Exception(ex.Message, ex);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
        return connection;
    }
}
﻿using FedttoejMvc.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class VideoViewModel
    {
        public static VideoViewModel ToViewModel(Video video)
        {
            return new VideoViewModel
            {
                Id = video.Id,
                Title = video.Title,
                Url = video.Url,
                Body = video.Body,
                YoutubeUrl = video.YoutubeUrl,
                CreatedDate = video.CreatedDate,
                PublishDateFriendly = video.PublishDateFriendly,
                PublishDateRobot = video.PublishDateRobot,
                MetaDescription = video.MetaDescription,
                MetaKeywords = video.MetaKeywords,
                PictureName = video.PictureName
            };
        }


        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }

        [AllowHtml]
        public string Body { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PublishDateFriendly { get; set; }
        public string PublishDateRobot { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string PictureName { get; set; }
        public string YoutubeUrl { get; set; }
        public List<Product> Products { get; set; }
        public Controller Controller { get; set; }
    }
}
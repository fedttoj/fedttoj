﻿using FedttoejMvc.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class AreaViewModel
    {
        public List<Product> Products { get; set; }
        public List<GetBestReducedPricesPercentResult> ReducedProductsPercent { get; set; }
        public List<GetBestReducedPricesKrResult> ReducedProductsKr { get; set; }


        
        public Controller Controller { get; set; }
        public string CurrentUrl { get; set; }
        public int CurrentPageNumber { get; set; }
        public Dictionary<string, int> Paging { get; set; }
    }
}
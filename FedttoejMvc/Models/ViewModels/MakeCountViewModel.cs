﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FedttoejMvc.Models.Data;

namespace FedttoejMvc.Models.ViewModels
{
    public class MakeCountViewModel
    {
        public FedtMake Make { get; set; }
        public int Count { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Models.Data;

namespace FedttoejMvc.Models.ViewModels
{
    public class CategoryViewModel
    {
        public List<Product> Products { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public string CurrentUrl { get; set; }
        public List<MakeCountViewModel> Makes { get; set; }
        public Dictionary<string, int> Paging { get; set; }
        public Controller Controller { get; set; }
        public Utilities.Area Area { get; set; }
        public int CategoryId { get; set; }
        public int MainCategoryId { get; set; }
        public string CategoryName { get; set; }
        public string MainCategoryName { get; set; }
        public int CurrentPageNumber { get; set; }
        public string AreaName { get; set; }
        public string AreaUrl { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public SortType Sorting { get; set; }
        public Dictionary<int, string> SortTypes { get; set; }

        public CategoryViewModel()
        {
            Description = "";
        }
    }

}
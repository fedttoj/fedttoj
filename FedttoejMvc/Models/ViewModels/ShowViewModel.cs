﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Models.Data;

namespace FedttoejMvc.Models.ViewModels
{
    public class ShowViewModel
    {
        public FedtFetchProductResult Product { get; set; }
        public List<Product> Products { get; set; }
        public List<Offer> Offers { get; set; }
        public List<MakeCountViewModel> Makes { get; set; }
        public string AreaName { get; set; }
        public string AreaUrl { get; set; }
        public string CategoryName { get; set; }
        public string CategoryUrl { get; set; }
        public string MainCategoryName { get; set; }
        public string MainCategoryUrl { get; set; }
        public string MakeUrl { get; set; }
        public bool FreePostage { get; set; }
        public Controller Controller { get; set; }
        public string PinterestUrl { get; set; }
        public string PinterestTitle { get; set; }
        public string PinterestImage { get; set; }
    }
}
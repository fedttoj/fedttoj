﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Models.Data;

namespace FedttoejMvc.Models.ViewModels
{
    public class MakesViewModel
    {
        public List<MakeCountViewModel> Makes { get; set; }
        public Controller Controller { get; set; }
        public int MainCategoryId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Utilities.Area Area { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public SortType Sorting { get; set; }
    }
}
﻿using FedttoejMvc.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class FrontPageViewModel
    {
        public List<Product> KidsProducts { get; set; }
        public List<Product> WomensProducts { get; set; }
        public List<Product> MensProducts { get; set; }
        public Controller Controller { get; set; }

        public List<GetBestReducedPricesPercentResult> ReducedProductsPercentWomen { get; set; }
        public List<GetBestReducedPricesKrResult> ReducedProductsKrWomen { get; set; }
        public List<GetBestReducedPricesPercentResult> ReducedProductsPercentMen { get; set; }
        public List<GetBestReducedPricesKrResult> ReducedProductsKrMen { get; set; }
        public List<GetBestReducedPricesPercentResult> ReducedProductsPercentKids { get; set; }
        public List<GetBestReducedPricesKrResult> ReducedProductsKrKids { get; set; }
        public List<ArticleViewModel> Articles1 { get; set; }
        public List<ArticleViewModel> Articles2 { get; set; }
        public VideoViewModel Video { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Models.ViewModels
{
    public class MenuViewModel
    {
        public List<MenuItem> MainCategoriesWomen { get; set; }
        public List<MenuItem> MainCategoriesMen { get; set; }
        public List<MenuItem> MainCategoriesKids { get; set; }
    }

    public class MenuItem
    {
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public List<MenuItem> SubCategories { get; set; }
        public string PreText { get; set; }
    }
}
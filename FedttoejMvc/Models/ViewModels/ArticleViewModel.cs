﻿using FedttoejMvc.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class ArticleViewModel
    {
        public static ArticleViewModel ToViewModel(Article article)
        {
            return new ArticleViewModel
            {
                Id = article.Id,
                Title = article.Title,
                Url = article.Url,
                Body = article.Body,
                BodyMobile = article.BodyMobile,
                CreatedDate = article.CreatedDate,
                PublishDateFriendly = article.PublishDateFriendly,
                PublishDateRobot = article.PublishDateRobot,
                MetaDescription = article.MetaDescription,
                MetaKeywords = article.MetaKeywords,
                PictureName = article.PictureName
            };
        }


        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }

        [AllowHtml]
        public string Body { get; set; }
        [AllowHtml]
        public string BodyMobile { get; set; }
        public DateTime CreatedDate { get; set; }
        public string PublishDateFriendly { get; set; }
        public string PublishDateRobot { get; set; }
        public string MetaDescription { get; set; }
        public string MetaKeywords { get; set; }
        public string PictureName { get; set; }
    }
}
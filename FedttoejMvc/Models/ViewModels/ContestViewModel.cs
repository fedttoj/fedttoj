﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Models.ViewModels
{
    public class ContestViewModel
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string Story { get; set; }
    }
}
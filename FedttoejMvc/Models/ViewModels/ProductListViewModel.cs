﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class ProductListViewModel
    {
        public List<Product> Products { get; set; }
        public Controller Controller { get; set; }
    }
}
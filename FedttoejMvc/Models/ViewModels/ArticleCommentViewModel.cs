﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FedttoejMvc.Models.ViewModels
{
    public class ArticleCommentViewModel
    {
        public int ArticleId { get; set; }
        public string UserName { get; set; }
        public string Comment { get; set; }
        public string Email { get; set; }
        public string Url { get; set; }

    }
}

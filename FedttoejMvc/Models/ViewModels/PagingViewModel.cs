﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Models.ViewModels
{
    public class PagingViewModel
    {
        public Dictionary<string, int> Pages { get; set; }
        public string Link { get; set; }
        public int CurrentPageNumber { get; set; }
    }
}
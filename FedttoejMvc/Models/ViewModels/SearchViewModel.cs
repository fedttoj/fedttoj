﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FedttoejMvc.Models.ViewModels
{
    public class SearchViewModel
    {
        public List<Product> Products { get; set; }
        public Dictionary<string, int> Paging { get; set; }
        public string SearchString { get; set; }
        public string Title { get; set; }
        public string PageDescription { get; set; }
        public Controller Controller { get; set; }
        public string CurrentUrl { get; set; }
        public int CurrentPageNumber { get; set; }
        public int PriceFrom { get; set; }
        public int PriceTo { get; set; }
        public SortType Sorting { get; set; }
        public Dictionary<int, string> SortTypes { get; set; }
    }
}
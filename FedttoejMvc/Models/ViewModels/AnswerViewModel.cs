﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FedttoejMvc.Models.Data;

namespace FedttoejMvc.Models.ViewModels
{
    public class AnswerViewModel
    {
        public List<FedtFetchAnswersResult> Answers { get; set; }
        public FedtFetchQuestionResult Question { get; set; }

    }
}
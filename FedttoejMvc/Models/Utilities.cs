﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;

namespace FedttoejMvc.Models
{
    public static class Utilities
    {
        public enum Area
        {
            Men = 1,
            Women = 2,
            Kids = 3,
            Diverse = 4,
            All = 5
        }

        public static string GetAreaString(Area area)
        {
            var result = "mand";
            switch (area)
            {
                case Area.Women:
                    result = "kvinde";
                    break;
                case Area.Kids:
                    result = "barn";
                    break;
                case Area.Diverse:
                    result = "diverse";
                    break;

            }
            return result;
        }

        public static Area GetAreaFromString(string area)
        {
            var result = Area.Women;
            switch (area)
            {
                case "mand":
                    result = Area.Men;
                    break;
                case "barn":
                    result = Area.Kids;
                    break;
                case "diverse":
                    result = Area.Diverse;
                    break;

            }
            return result;
        }

        public static string GetAreaStringLong(Area area)
        {
            var result = "herretøj";
            switch (area)
            {
                case Area.Women:
                    result = "kvindetøj";
                    break;
                case Area.Kids:
                    result = "børnetøj";
                    break;
                case Area.Diverse:
                    result = "";
                    break;

            }
            return result;
        }

        public static string Urlencode(string name)
        {
            name = name.Replace("Ã¸", "o");
            name = name.Replace("‡", "");
            name = name.Replace("|", "");
            name = name.Replace("´", "");
            name = name.Replace("`", "");
            name = name.Replace("º", "");
            name = name.Replace("°", "");
            name = name.Replace("#", "");
            name = name.Replace("™", "");
            name = name.Replace("ß", "ss");
            name = name.Replace("ñ", "n");
            name = name.Replace("ø", "oe");
            name = name.Replace("é", "e");
            name = name.Replace("è", "e");
            name = name.Replace("È", "E");
            name = name.Replace("Ê", "E");
            name = name.Replace("É", "E");
            name = name.Replace("Ë", "E");
            name = name.Replace("Í", "I");
            name = name.Replace("Ö", "O");
            name = name.Replace("Ä", "A");
            name = name.Replace("Â", "A");
            name = name.Replace("ë", "e");
            name = name.Replace("ã", "a");
            name = name.Replace("Ñ", "N");
            name = name.Replace("Ã", "A");
            name = name.Replace("î", "i");
            name = name.Replace("ê", "e");
            name = name.Replace("ó", "o");
            name = name.Replace("ô", "o");
            name = name.Replace("Ó", "O");
            name = name.Replace("í", "i");
            name = name.Replace("ï", "i");
            name = name.Replace("á", "a");
            name = name.Replace("à", "a");
            name = name.Replace("À", "A");
            name = name.Replace("ç", "c");
            name = name.Replace("æ", "ae");
            name = name.Replace("ü", "u");
            name = name.Replace("û", "u");
            name = name.Replace("ö", "o");
            name = name.Replace("ò", "o");
            name = name.Replace("ä", "a");
            name = name.Replace("å", "aa");
            name = name.Replace("Ø", "Oe");
            name = name.Replace("Å", "Aa");
            name = name.Replace("Ü", "U");
            name = name.Replace("Æ", "Ae");
            name = name.Replace("+", "-");
            name = name.Replace(" ", "-");
            name = name.Replace("/", "-");
            name = name.Replace("&amp;", "og");
            name = name.Replace("&", "og");
            name = name.Replace(">", "");
            name = name.Replace("<", "");
            name = name.Replace("$", "S");
            name = name.Replace("=", "");
            name = name.Replace("¯", "");
            name = name.Replace(",", "");
            name = name.Replace("'", "");
            name = name.Replace(";", "");
            name = name.Replace("(", "");
            name = name.Replace(")", "");
            name = name.Replace("®", "");
            name = name.Replace("½", "");
            name = name.Replace(".", "");
            name = name.Replace("*", "");
            name = name.Replace("\"", "");
            name = name.Replace("\\", "-");
            name = name.Replace("+", "");
            name = name.Replace("%", "");
            name = name.Replace("?", "");
            name = name.Replace("--", "-");
            name = name.Replace("£", "");
            name = name.Replace(".", "");
            name = name.Replace("---", "-");
            name = name.Replace("--", "-");
            name = name.Replace("]", "");
            name = name.Replace("[", "");
            name = name.Replace("Á", "A");
            name = name.Replace("æ", "ae");
            name = name.Replace("œ", "ae");
            name = name.Replace("å", "aa");
            name = name.Replace("¸", "");
            name = name.Replace(".", "");
            name = name.Replace("·", "");
            name = name.Replace("‘", "");
            name = name.Replace(":", "");
            name = name.Replace("\u008D", "");
            name = name.Replace("\u00A0", "");
            name = name.Replace("–", "");
            name = name.Replace("¨", "");
            name = name.Replace("”", "");
            name = name.Replace("³", "");
            name = name.Replace("¥", "");
            name = name.Replace("¦", "");
            name = name.Replace("Û", "U");
            name = name.Replace("©", "");
            name = name.Replace("’", "");
            name = name.Replace("×", "");
            name = name.Replace("²", "");
            name = name.Replace("“", "");
            name = name.Replace("„", "");
            name = name.Replace("¾", "");
            name = name.Replace("—", "-");
            name = name.Replace("ú", "u");
            name = name.Replace("«", "-");
            name = name.Replace("ì", "i");
            name = name.Replace("¼", "1-4");
            name = name.Replace("½", "1-2");
            name = name.Replace("÷", "");
            name = name.Replace("Ç", "C");
            name = name.Replace("Ú", "U");
            name = name.Replace("â", "a");
            name = name.Replace("š", "s");
            name = name.Replace("ÿ", "y");
            name = name.Replace("ª", "a");
            name = name.Replace("ù", "u");
            name = name.Replace("@", "a");
            name = name.Replace("{", "");
            name = name.Replace("¶", "");
            name = name.Replace("¤", "");
            name = name.Replace("ž", "z");
            name = name.Replace("‚", "");
            name = name.Replace("Ž", "Z");
            name = name.Replace("¶", "");
            name = name.Replace("Š", "");

            return name;
        }
        
        public static Dictionary<int, string> MainCategories()
        {
            var mainCategories = new Dictionary<int, string>();
            mainCategories.Add(0, "Herretøj");
            mainCategories.Add(1, "Bukser og jeans");
            mainCategories.Add(2, "Shirts");
            mainCategories.Add(3, "Jakker");
            mainCategories.Add(4, "Bluser og trøjer");
            mainCategories.Add(5, "Shorts");
            mainCategories.Add(6, "Diverse");
            mainCategories.Add(7, "Sko og støvler");
            mainCategories.Add(8, "Undertøj");
            mainCategories.Add(9, "Skjorter");
            mainCategories.Add(10, "Nederdele");
            mainCategories.Add(11, "Badetøj");
            mainCategories.Add(12, "Toppe");
            mainCategories.Add(13, "Kjoler");
            mainCategories.Add(14, "Dragter og overalls");
            mainCategories.Add(15, "Herretøj");
            mainCategories.Add(16, "Dametøj");
            mainCategories.Add(17, "Tøj");
            mainCategories.Add(18, "Sportstøj");
            return mainCategories;
        }

        public static string StripHtml(string htmlString)
        {
            const string pattern = @"<(.|\n)*?>";
            return Regex.Replace(htmlString, pattern, string.Empty);
        }

        public static int GetTestVariant(int splitTestNumber, int numberOfVariants, bool showOrClick)
        {
            var result = 0;
            if (HttpContext.Current.Session["Splittest" + splitTestNumber] != null)
                result = int.Parse(HttpContext.Current.Session["Splittest" + splitTestNumber].ToString());
            if (result == 0 && showOrClick)
            {
                result = (DateTime.Now.Second % numberOfVariants) + 1;
                HttpContext.Current.Session["Splittest" + splitTestNumber] = result.ToString();
            }
            return result;
        }

        public static bool TestForLinkError(string url, Area area, string title)
        {
            if (area != Area.Men && url.ToLower().Contains("herretoej"))
            {
                return true;
            }
            if (area != Area.Men && url.ToLower().Contains("/mand/"))
            {
                return true;
            }
            if (area != Area.Women && url.ToLower().Contains("/kvinde/"))
            {
                return true;
            }
            if (area != Area.Kids && url.ToLower().Contains("/barn/"))
            {
                return true;
            }
            if (area != Area.Women && area != Area.Diverse && url.ToLower().Contains("dametoej"))
            {
                return true;
            }

            if (title != "" &&
                url.Contains(HttpUtility.UrlEncode(title.Replace(":", "--").Replace("/", "").Replace("%2f", ""))) &&
                Urlencode(title.Replace(":", "--").Replace("/", "").Replace("%2f", "")) != HttpUtility.UrlEncode(title.Replace(":", "--").Replace("/", "").Replace("%2f", "")))
                return true;
            return false;
        }

        public static int DownloadFile(String remoteFilename, String localFilename)
        {
            // Function will return the number of bytes processed
            // to the caller. Initialize to 0 here.
            int bytesProcessed = 0;

            // Assign values to these objects here so that they can
            // be referenced in the finally block
            Stream remoteStream = null;
            Stream localStream = null;
            WebResponse response = null;

            // Use a try/catch/finally block as both the WebRequest and Stream
            // classes throw exceptions upon error
            try
            {
                // Create a request for the specified remote file name
                var request = WebRequest.Create(remoteFilename);

                // Send the request to the server and retrieve the
                // WebResponse object 
                response = request.GetResponse();

                // Once the WebResponse object has been retrieved,
                // get the stream object associated with the response's data
                remoteStream = response.GetResponseStream();

                // Create the local file
                localStream = File.Create(localFilename);

                // Allocate a 1k buffer
                var buffer = new byte[1024];
                int bytesRead;

                // Simple do/while loop to read from stream until
                // no bytes are returned
                do
                {
                    // Read data (up to 1k) from the stream
                    bytesRead = remoteStream.Read(buffer, 0, buffer.Length);

                    // Write the data to the local file
                    localStream.Write(buffer, 0, bytesRead);

                    // Increment total bytes processed
                    bytesProcessed += bytesRead;
                } while (bytesRead > 0);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                // Close the response and streams objects here 
                // to make sure they're closed even if an exception
                // is thrown at some point
                if (response != null) response.Close();
                if (remoteStream != null) remoteStream.Close();
                if (localStream != null) localStream.Close();
            }

            // Return total bytes processed to caller.
            return bytesProcessed;
        }

        public static string ReplaceDesciptionText(string text)
        {
            text = text.Replace("~", ", ");
            text = text.Replace(" cm og er fotograferet i str. ", " cm høj, og tøjet er størrelse ");
            text = text.Replace("Fotomodellen måler ", "Modellen på billedet er ");
            text = text.Replace("Fotomodellen er ", "Modellen på billedet er ");
            text = text.Replace("Tøj nu gratis levering", "Alt tøj leveres gratis");
            text = text.Replace("gratis levering", "leveres gratis");
            text = text.Replace("ægte", "rigtig");
            text = text.Replace("oversize model", "stor model");
            text = text.Replace("med slid ", "med et slidt look ");
            text = text.Replace("Lækker ", "Dejlig ");
            text = text.Replace("øverst", "for oven");
            text = text.Replace("i siderne", "i siden");
            text = text.Replace("Fremstillet af 100%", "Lavet af");
            text = text.Replace("Fremstillet af 99%", "Lavet af");
            text = text.Replace("Fremstillet af 98%", "Lavet af");
            text = text.Replace("Materiale:", "Lavet af");
            text = text.Replace("100%", "lavet af");
            text = text.Replace("99%", "lavet af");
            text = text.Replace("98%", "lavet af");
            text = text.Replace("foran", "i fronten");
            text = text.Replace("nederst", "for neden");
            text = text.Replace("Desuden", "Derudover");
            text = text.Replace("Kvaliteten", "Den");
            text = text.Replace("dame ", "kvinde");
            text = text.Replace("str. ", "størrelse ");
            text = text.Replace(" str ", " størrelse ");
            text = text.Replace("rundet halsudskæring", "rund hals");
            text = text.Replace("rundhalsudskæring", "rund hals");
            text = text.Replace("Længden er ", "I længden er den ");
            text = text.Replace("hætte sweatshirt ", "hættesweatshirt");
            text = text.Replace(" buks ", " bukser ");
            text = text.Replace(" skæve syninger ", " skrå syninger ");
            text = text.Replace(" & ", " og ");
            text = text.Replace("Maskinvask ", "Kan vaskes ved ");


            return text;
        }


        public static int GetQueryInt(string key)
        {
            var value = HttpContext.Current.Request.QueryString[key];
            if (string.IsNullOrEmpty(value))
                value = HttpContext.Current.Request.RequestContext.RouteData.Values[key] as string;
            if (string.IsNullOrEmpty(value)) return 0;

            int parsedValue;
            return int.TryParse(value, out parsedValue) ? parsedValue : GetIntFromString(value);
        }



        public static long GetQueryLong(string key)
        {
            var value = HttpContext.Current.Request.QueryString[key];
            if (string.IsNullOrEmpty(value))
                value = HttpContext.Current.Request.RequestContext.RouteData.Values[key] as string;
            if (string.IsNullOrEmpty(value)) return 0;

            long parsedValue;
            return long.TryParse(value, out parsedValue) ? parsedValue : 0;
        }

        public static string GetQueryString(string key)
        {
            var value = HttpContext.Current.Request.QueryString[key];
            if (!string.IsNullOrEmpty(value))
                return value;
            value = HttpContext.Current.Request.RequestContext.RouteData.Values[key] as string;
            return !string.IsNullOrEmpty(value) ? value : "";
        }

        public static bool GetQueryBool(string key)
        {
            var value = HttpContext.Current.Request.QueryString[key];
            if (string.IsNullOrEmpty(value))
                value = HttpContext.Current.Request.RequestContext.RouteData.Values[key] as string;
            if (string.IsNullOrEmpty(value)) return false;

            switch (value.ToLower())
            {
                case "0":
                case "false":
                    return false;
                case "1":
                case "true":
                    return true;
            }
            return false;
        }

        public static int GetIntFromString(string str)
        {
            var result = "0";
            var arr = str.ToCharArray();
            var i = 0;
            while (i < arr.Length && "0123456789".Contains(arr[i].ToString()))
            {
                result += arr[i].ToString();
                i++;
            }

            return int.Parse(result);
        }

        public static bool IsMobile()
        {
            var userAgent = HttpContext.Current.Request.UserAgent;

            //if (HttpContext.Current.Request.Browser.IsMobileDevice)
            //    return true;

            if (string.IsNullOrEmpty(userAgent))
                return false;

            //if (IsBrowserIpad(userAgent))
            //    return true;

            if (userAgent.ToLower().Contains("googlebot") && userAgent.ToLower().Contains("mobile"))
                return true;

            if (userAgent.ToLower().Contains("android"))
                return true;

            //userAgent = userAgent.ToLower();
            //return MobileDevices.Any(x => userAgent.Contains(x));

            return IsBrowserMobile(userAgent);
        }

        private static readonly Regex MobileDetectionRegexOs = new Regex(@"(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino", RegexOptions.IgnoreCase | RegexOptions.Multiline);
        private static readonly Regex MobileDetectionRegex = new Regex(@"1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-", RegexOptions.IgnoreCase | RegexOptions.Multiline);


        private static bool IsBrowserMobile(string userAgent)
        {
            //from http://detectmobilebrowsers.com/

            return (MobileDetectionRegexOs.IsMatch(userAgent) || MobileDetectionRegex.IsMatch(userAgent.Substring(0, 4)));
        }

        private static bool IsBrowserIpad(string userAgent)
        {
            return userAgent.ToLower().Contains("ipad");
        }

        public static string LimitToLengthWithDots(this string self, int length, bool addDots = true)
        {
            if (self.Length <= length + 1)
                return self;
            if (addDots)
                return self.Length >= length ? string.Format("{0}...", self.Substring(0, length - 1)) : self;
            return self.Length >= length ? string.Format("{0}", self.Substring(0, length - 1)) : self;
        }

        public static string Capitalize(this string s)
        {
            if (string.IsNullOrEmpty(s))
                return "";

            return s[0].ToString(CultureInfo.InvariantCulture).ToUpper() + s.Remove(0, 1);
        }

        public static int GetProductCount(string mapPath)
        {
            var result = 0;
            var productCount = HttpContext.Current.Cache["ProductCount"];
            if (productCount == null)
            {
                var query = new ProductSearchQuery();
                query.SearchString = "til";
                query.PageNumber = 1;
                query.HitsPerPage = 1;
                query.SpecificSearch = false;
                query.TheArea = Utilities.Area.All;
                query.IndexPath = mapPath;
                var products = Product.SearchLucene(query);
                result = products.ReturnedRows;
                HttpContext.Current.Cache.Insert("ProductCount", result, null, DateTime.Now.AddHours(2), Cache.NoSlidingExpiration);
            }
            else
                result = (int) productCount;

            return result;
        }

        public static ProductSearchQuery DoSearch(Area theArea, int makeId, int mainCategoryId, int categoryId, int pageSize, SortType sort, int pageNumber, string lucenePath, int priceFrom, int priceTo, string search = "", int maxHits = 0)
        {
            var query = new ProductSearchQuery();
            query.TheArea = theArea;
            query.PageNumber = pageNumber;
            query.MakeId = makeId;
            query.MainCategoryId = mainCategoryId;
            query.HitsPerPage = pageSize;
            query.CategoryId = categoryId;
            query.SearchString = search;
            query.SortBy = sort;
            query.MaxHits = maxHits;
            query.PriceFrom = priceFrom;
            query.PriceTo = priceTo;
            //query.IndexPath = Server.MapPath("~/Indexer/LuceneIndex");
            query.IndexPath = lucenePath;
            return Product.SearchLucene(query);
        }

        public static CategorySearchQuery FindCategoriesAndMakes(Utilities.Area theArea, int mainCategoryId, int categoryId, string lucenePath, bool overrideCache = false)
        {
            //if (theArea == Utilities.Area.Diverse)
            //    mainCategoryId = 0;
            var query = new CategorySearchQuery();
            query.TheArea = theArea;
            query.MainCategoryId = mainCategoryId;
            query.CategoryId = categoryId;
            query.SearchString = "";
            query.IndexPath = lucenePath;
            return Category.SearchLucene(query, overrideCache);
        }

        public static string GetSearchPhrase(string name)
        {
            name = name.Trim();
            name = name.Replace("+", "-");
            name = name.Replace(" ", "-");
            name = name.Replace("/", "-");
            name = name.Replace("&", "og");
            name = name.Replace(">", "");
            name = name.Replace("<", "");
            name = name.Replace("$", "");
            name = name.Replace("=", "");
            name = name.Replace("¯", "");
            name = name.Replace(",", "");
            name = name.Replace("'", "");
            name = name.Replace(";", "");
            name = name.Replace("(", "");
            name = name.Replace(")", "");
            name = name.Replace("½", "");
            name = name.Replace(".", "");
            name = name.Replace("*", "");
            name = name.Replace("\"", "");
            name = name.Replace("\\", "-");
            name = name.Replace("+", "");
            name = name.Replace("%", "");
            name = name.Replace("?", "");
            name = name.Replace("---", "-");
            name = name.Replace("--", "-");
            name = name.Replace("£", "");
            name = name.Replace(".", "");
            name = name.Replace("]", "");
            name = name.Replace("[", "");
            name = name.Replace("¸", "");
            name = name.Replace(".", "");
            name = name.Replace("¨", "");
            name = name.Replace("¶", "");
            name = name.Replace("¤", "");
            name = name.Replace("´", "");
            name = name.Replace(":", "").ToLower();
            return name;
        }

        public static string GetSearchQuery(int priceFrom, int priceTo, SortType sorting)
        {
            var query = "";
            
            if (priceTo > 0)
            {
                query = "?priceFrom=" + priceFrom + "&priceTo=" + priceTo;
            }

            if (sorting != SortType.Id)
            {
                if (priceTo > 0)
                    query += "&";
                else
                    query += "?";

                query += "sorting=" + (int)sorting;
            }
         
            return query;
        }
    }
}
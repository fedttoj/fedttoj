﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FedttoejMvc
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }


        void Application_BeginRequest(object sender, EventArgs e)
        {
            System.Web.HttpContext httpContext = HttpContext.Current;
            var url = httpContext.Request.Url.AbsoluteUri;
        
            if (url.Contains("http://") && !url.Contains("localhost"))
                Response.RedirectPermanent("https://fedttoj.dk" + httpContext.Request.RawUrl, true);          

            if ((!url.Contains("fedttoj.dk") || url.Contains("www.")) && !url.Contains("localhost"))
                Response.RedirectPermanent("https://fedttoj.dk" + httpContext.Request.RawUrl, true);             
        }
    }
}

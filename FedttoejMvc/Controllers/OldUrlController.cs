﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;

namespace FedttoejMvc.Controllers
{
    public partial class OldUrlController : Controller
    {
        private string SearchText
        {
            get { return Request.QueryString["SearchString"]; }

        }

        [Route("{gender}/maerker/{cat}/{mainId}-{categoryId}-{make}")]
        public virtual ActionResult MainCategoryWithMakeOld(string gender, string cat, int mainId, int categoryId, string make)
        {
            return RedirectPermanent(Url.Action(MVC.Home.OldMainCategoryWithMake(gender, cat, mainId, categoryId)) + "?make=" + HttpUtility.UrlEncode(make));
        }

        [Route("{gender}/maerker/{cat}/{subcat}/0-{categoryIdAndmake}", Order = 1)]
        public virtual ActionResult CategoryWithMakeOld(string gender, string cat, string subcat, string categoryIdAndmake)
        {
            var categoryId = Utilities.GetIntFromString(categoryIdAndmake);
            var make = categoryIdAndmake.Replace(categoryId + "-", "");
            return RedirectPermanent(Url.Action(MVC.Home.OldCategoryWithMake(gender, cat, subcat, categoryId)) + "?make=" + HttpUtility.UrlEncode(make));
        }

        [Route("{gender}/maerker/{cat}/{subcat}/0-{categoryId}-{make}", Order = 2)]
        public virtual ActionResult CategoryWithMakeOld(string gender, string cat, string subcat, int categoryId, string make)
        {
            return RedirectPermanent(Url.Action(MVC.Home.OldCategoryWithMake(gender, cat, subcat, categoryId)) + "?make=" + HttpUtility.UrlEncode(make));
        }

        [Route("sitemap.aspx")]
        public virtual ActionResult SitemapOld()
        {
            return RedirectPermanent(Url.Action(MVC.Home.SiteMap()));
        }

        [Route("about.aspx")]
        public virtual ActionResult AboutOld()
        {
            return RedirectPermanent(Url.Action(MVC.Home.About()));
        }

        [Route("contact.aspx")]
        public virtual ActionResult ContactOld()
        {
            return RedirectPermanent(Url.Action(MVC.Home.Contact()));
        }

        [Route("searchresult.aspx")]
        public virtual ActionResult SearchResultOld()
        {
            return RedirectPermanent("/");
        }

        [Route("redirectErrors.aspx")]
        public virtual ActionResult RedirectErrors()
        {
            var query = Request.QueryString[0];
            var index = query.LastIndexOf("/", System.StringComparison.Ordinal);
            var substring = query.Substring(index+1, query.Length - index-1);
            var id = Utilities.GetIntFromString(substring);
           
            var product = Product.GetProduct(id, Server.MapPath("~/Indexer/LuceneIndex"));
            if (product != null && product.Id > 0)
                return RedirectPermanent(GetUrlHelper.GetProductUrl(this, product));


            return RedirectPermanent(Url.Action((MVC.Home.Index())));
        }
    }
}
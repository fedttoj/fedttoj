﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Caching;
using System.Web.Mvc;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Models.ViewModels;


namespace FedttoejMvc.Controllers
{
    public partial class ArticleController : Controller
    {

      

        //public virtual ActionResult ArticleComment(int articleId, string url)
        //{
        //    List<ArticleComment> comments;
        //    using (var db = new TablesDataContext())
        //    {
        //        comments = db.ArticleComments.Where(x => x.ArticleId == articleId).ToList();
        //    }

        //    @ViewBag.Url = url;
        //    @ViewBag.ArticleId = articleId;

        //    return PartialView(comments);
        //}

        [HttpPost, Route("artikler/post/en-artikel/nu")]
        public virtual ActionResult PostArticleComment(ArticleCommentViewModel viewModel)
        {
            if (!string.IsNullOrWhiteSpace(viewModel.Email))
            {
                using (var db = new TablesDataContext())
                {
                    db.FedtSaveArticleComment(viewModel.UserName, viewModel.Comment, viewModel.ArticleId);
                }
            }
            return Redirect(viewModel.Url);
        }
        

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using System.Web.Caching;
using FedttoejMvc.Models.ViewModels;
using System.Web.Routing;
using static FedttoejMvc.Models.Utilities;

namespace FedttoejMvc.Controllers
{
    public partial class HomeController : Controller
    {
        private int PageSize = 36;
        private static Dictionary<int, string> _mainCategories = Utilities.MainCategories();
        private int PageNumber
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["Page"]) ? int.Parse(Request.QueryString["Page"]) : 1; }
        }

        private int CacheBreaker
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["cacheBreaker"]) ? int.Parse(Request.QueryString["cacheBreaker"]) : 0; }
        }

        [OutputCache(Duration = 604800)]
        public virtual ActionResult MobileMenu()
        {
            var mainMenu = (MenuViewModel)System.Web.HttpContext.Current.Cache["MainMenu-container"];

            if (mainMenu == null)
                mainMenu = BuildMobileMenu();

            return PartialView(mainMenu);
        }

        public virtual ActionResult Index()
        {
            var url = HttpContext.Request.RawUrl.ToLower();
            if (url != "/")
                return RedirectPermanent("/");

            SetGeneralValues(Utilities.Area.Women, 13, "", "");
       
            var kidsNewest = (List<Product>)System.Web.HttpContext.Current.Cache["kidsNewest"];
            var womenNewest =  (List<Product>)System.Web.HttpContext.Current.Cache["womenNewest"];
            var menNewest =  (List<Product>)System.Web.HttpContext.Current.Cache["menNewest"];
            var productsPercentWoman = new List<GetBestReducedPricesPercentResult>();
            var productsKrWoman = new List<GetBestReducedPricesKrResult>();
            var productsPercentMan = new List<GetBestReducedPricesPercentResult>();
            var productsKrMan = new List<GetBestReducedPricesKrResult>();
            var productsPercentKids = new List<GetBestReducedPricesPercentResult>();
            var productsKrKids = new List<GetBestReducedPricesKrResult>();

            if (kidsNewest == null)
            {
                kidsNewest = Utilities.DoSearch(Utilities.Area.Kids, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("kidsNewest", kidsNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            if (womenNewest == null)
            {
                womenNewest = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("womenNewest", womenNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            if (menNewest == null)
            {
                menNewest = Utilities.DoSearch(Utilities.Area.Men, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("menNewest", menNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            //using (var db = new TablesDataContext())
            //{
            //    productsPercentWoman = (List<GetBestReducedPricesPercentResult>)System.Web.HttpContext.Current.Cache["ReducedPercent" + Utilities.Area.Women];
            //    productsKrWoman = (List<GetBestReducedPricesKrResult>)System.Web.HttpContext.Current.Cache["ReducedKr" + Utilities.Area.Women];
            //    productsPercentMan = (List<GetBestReducedPricesPercentResult>)System.Web.HttpContext.Current.Cache["ReducedPercent" + Utilities.Area.Men];
            //    productsKrMan = (List<GetBestReducedPricesKrResult>)System.Web.HttpContext.Current.Cache["ReducedKr" + Utilities.Area.Men];
            //    productsPercentKids = (List<GetBestReducedPricesPercentResult>)System.Web.HttpContext.Current.Cache["ReducedPercent" + Utilities.Area.Kids];
            //    productsKrKids = (List<GetBestReducedPricesKrResult>)System.Web.HttpContext.Current.Cache["ReducedKr" + Utilities.Area.Kids];

            //    if (productsPercentWoman == null)
            //    {
            //        productsPercentWoman = db.GetBestReducedPricesPercent(200, (int)Utilities.Area.Women).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Women, productsPercentWoman, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }

            //    if (productsKrWoman == null)
            //    {
            //        productsKrWoman = db.GetBestReducedPricesKr(200, (int)Utilities.Area.Women).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Women, productsKrWoman, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }

            //    if (productsPercentMan == null)
            //    {
            //        productsPercentMan = db.GetBestReducedPricesPercent(200, (int)Utilities.Area.Men).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Men, productsPercentMan, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }

            //    if (productsKrMan == null)
            //    {
            //        productsKrMan = db.GetBestReducedPricesKr(200, (int)Utilities.Area.Men).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Men, productsKrMan, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }

            //    if (productsPercentKids == null)
            //    {
            //        productsPercentKids = db.GetBestReducedPricesPercent(200, (int)Utilities.Area.Kids).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Kids, productsPercentKids, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }

            //    if (productsKrKids == null)
            //    {
            //        productsKrKids = db.GetBestReducedPricesKr(200, (int)Utilities.Area.Kids).ToList();
            //        System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Kids, productsKrKids, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            //    }
            //}

            var viewModel = new FrontPageViewModel
            {
                KidsProducts = kidsNewest.OrderBy(x => Guid.NewGuid()).Take(8).ToList(),
                WomensProducts = womenNewest.OrderBy(x => Guid.NewGuid()).Take(8).ToList(),
                MensProducts = menNewest.OrderBy(x => Guid.NewGuid()).Take(8).ToList(),
                ReducedProductsPercentWomen = productsPercentWoman.Take(10).ToList(),
                ReducedProductsKrWomen = productsKrWoman.Take(10).ToList(),
                ReducedProductsPercentMen = productsPercentMan.Take(10).ToList(),
                ReducedProductsKrMen = productsKrMan.Take(10).ToList(),
                ReducedProductsPercentKids = productsPercentKids.Take(10).ToList(),
                ReducedProductsKrKids = productsKrKids.Take(10).ToList(),
                Articles1 = ArticleMethods.GetAllArticles().Take(2).ToList(),
                Articles2 = ArticleMethods.GetAllArticles().Skip(2).Take(3).ToList(),
                Video = VideoMethods.GetAllVideos().FirstOrDefault(),
                Controller = this
            };

            ViewBag.TheTitle = "Fedt tøj til mænd, kvinder og børn";
            ViewBag.MetaDescription = "Stort udvalg af fedt tøj til mænd, kvinder og børn. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
            ViewBag.MetaKeyWords = "tøj, herretøj, dametøj, mandetøj, dametøj, kvindetøj, børnetøj, drengetøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts, flyverdragter";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Index_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("temp")]
        public virtual ActionResult temp()
        {
            
            if (Utilities.IsMobile())
                return View("temp");
            return View("temp");
        }


        [Route("tidligere-sogninger")]
        public virtual ActionResult PreviousSearches()
        {
            var viewModel = new List<string>();
            var count = 0;
            using (var db = new  TablesDataContext())
            {
                List<SearchPage> firstPages = new List<SearchPage>();
                if (PageNumber == 1)
                {
                    firstPages = db.SearchPages.Where(x => x.PageDescription.Length > 10).ToList();
                }

                var pages = db.SearchPages.OrderByDescending(x => x.Id).Skip((PageNumber - 1) * 100).Take(100 - firstPages.Count);
                firstPages.AddRange(pages);
                foreach (var page in firstPages)
                {
                    viewModel.Add("<a href=\"" + Url.Action(MVC.Home.Search(page.SearchTerm)) + "\" title=\"" + GetSearchStringFromPhrase(page.SearchTerm) + "\">" + GetSearchStringFromPhrase(page.SearchTerm) + "</a>");
                }
                count = db.SearchPages.Count();
            }
            ViewBag.CurrentUrl = Url.Action(MVC.Home.PreviousSearches());
            ViewBag.CurrentPageNumber = PageNumber;
            ViewBag.Paging = ProductHelper.SetPaging(count, 50, PageNumber, Utilities.IsMobile());
            ViewBag.TheTitle = "Tidligere søgninger";
            if (PageNumber > 1)
                ViewBag.TheTitle += " - side " + PageNumber;
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.PreviousSearches_Mobile, viewModel);
            return View(MVC.Home.Views.PreviousSearches, viewModel);
        }
        

        [Route("s/{querystr}")]
        public virtual ActionResult Search(string queryStr)
        {
            queryStr = queryStr.Trim();
            var searchText = GetSearchStringFromPhrase(queryStr);
            if (searchText == "gucci tasker")
                Response.Redirect("/s/gucci-taske");

            if (searchText == "t shirt dame")
                Response.Redirect("/s/t-shirts-dame");

            ViewBag.SearchText = searchText;
            ViewBag.Controller = this;
            ViewBag.TheArea = Utilities.Area.All;
            ViewBag.MainCategoryName = _mainCategories[13];
            ViewBag.TheTitle = searchText;
            ViewBag.MetaDescription = "Find " + queryStr + " på fedttoj.dk";
            ViewBag.MetaKeyWords = queryStr.Replace(" ", ", ");
            if (PageNumber > 1)
                ViewBag.TheTitle = ViewBag.TheTitle + " - side " + PageNumber;
            
            ViewBag.MenuArea = Utilities.Area.All;
            var query = new ProductSearchQuery();
            query.SearchString = searchText.Replace(" og ", " ").Replace(" med ", " ").Replace(" the ", " ").Replace(" in ", " ");
            query.PageNumber = PageNumber;
            query.HitsPerPage = PageSize;
            query.TheArea = Utilities.Area.All;
            query.SortBy = (SortType)GetQueryInt("sorting");
            query.PriceFrom = GetQueryInt("priceFrom");
            query.PriceTo = GetQueryInt("priceTo");
            query.IndexPath = Server.MapPath("~/Indexer/LuceneIndex");

            if (query.SearchString.Contains("jakke") && !query.SearchString.Contains("jakker"))
                query.SearchString += " jakker";

            var result = Product.SearchLucene(query);

            SearchPage searchPage = null;
            if (result.ReturnedRows > 5 && queryStr.Length > 3)
            {
                searchPage = SaveSearchPage(queryStr);
            }
            var viewModel = new SearchViewModel
            {
                Products = result.Result,
                Paging = ProductHelper.SetPaging(result.ReturnedRows, PageSize, PageNumber, Utilities.IsMobile()),
                SearchString = searchText,               
                Controller = this,
                CurrentUrl = Url.Action(MVC.Home.Search(queryStr)),
                PriceFrom = query.PriceFrom,
                PriceTo = query.PriceTo,
                Sorting = query.SortBy,
            };

            viewModel.SortTypes = new Dictionary<int, string>();
            viewModel.SortTypes.Add(0, "Nyeste først");
            viewModel.SortTypes.Add(1, "Ældste først");
            viewModel.SortTypes.Add(2, "Billigste først");
            viewModel.SortTypes.Add(3, "Dyreste først");

            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
            {
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
                ViewBag.PrevPage = (PageNumber - 1).ToString();                
            }
            else
            {
                if (searchText == "gucci taske")
                {
                    ViewBag.GucciTaske = "true";
                    ViewBag.MetaDescription = "Gucci-håndtasker findes i mange forskellige størrelser og stilarter. De er små eller mellemstore, lavet af læder, lærred og ruskind og har rum med lynlås og metallås eller magnetiske kliklukninger";
                    ViewBag.TheArea = Utilities.Area.Women;
                    ViewBag.MenuArea = Utilities.Area.Women;
                    ViewBag.MainCategoryName = _mainCategories[6];
                    ViewBag.TheTitle = "Find din nye Gucci taske på fedttoj.dk";
                }
                else if (searchPage != null)
                {
                    if (!string.IsNullOrWhiteSpace(searchPage.Title))
                        ViewBag.TheTitle = searchPage.Title;

                    viewModel.Title = searchPage.Title;

                    if (!string.IsNullOrWhiteSpace(searchPage.MetaDescription))
                        ViewBag.MetaDescription = searchPage.MetaDescription;

                    viewModel.PageDescription = searchPage.PageDescription;
                }
            }

            ViewBag.TheTitle += " - fedttoj.dk";

            if (result.ReturnedRows > PageNumber * PageSize)
            {
                ViewBag.NextPage = (PageNumber + 1).ToString();
            }
            viewModel.CurrentPageNumber = PageNumber;
            Response.AppendHeader("Vary", "User-Agent");
            if (Utilities.IsMobile())
                return View(MVC.Search.Views.Search_Mobile, viewModel);
            return View(MVC.Search.Views.Search, viewModel);
        }

        private SearchPage SaveSearchPage(string queryStr)
        {
            using (var db = new TablesDataContext())
            {
                var searchPage = db.SearchPages.SingleOrDefault(x => x.SearchTerm == queryStr);
                if (searchPage == null)
                {
                    searchPage = new SearchPage { SearchTerm = queryStr, PageDescription = "", MetaDescription = "", Title = "", Count = 0 };
                    db.SearchPages.InsertOnSubmit(searchPage);
                    
                }
                searchPage.Count = searchPage.Count + 1;
                db.SubmitChanges();
                return searchPage;
            }            
        }

        [Route("app/kids")]
        public virtual ActionResult AppKids(int pageNumber)
        {
            var kidsNewest = (List<Product>)System.Web.HttpContext.Current.Cache["kidsNewest"];
            if (kidsNewest == null)
            {
                kidsNewest = Utilities.DoSearch(Utilities.Area.Kids, 0, 0, 0, 10000, SortType.Id, 1, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("kidsNewest", kidsNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            return Json(kidsNewest.Skip(pageNumber-1).Take(PageSize));
        }

        [Route("app/men")]
        public virtual ActionResult AppMen(int pageNumber)
        {
            var menNewest = (List<Product>)System.Web.HttpContext.Current.Cache["menNewest"];
            if (menNewest == null)
            {
                menNewest = Utilities.DoSearch(Utilities.Area.Men, 0, 0, 0, 10000, SortType.Id, 1, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("menNewest", menNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            return Json(menNewest.Skip(pageNumber - 1).Take(PageSize));
        }

        [Route("app/women")]
        public virtual ActionResult AppWomen(int pageNumber)
        {
            var womenNewest = (List<Product>)System.Web.HttpContext.Current.Cache["womenNewest"];
            if (womenNewest == null)
            {
                womenNewest = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 10000, SortType.Id, 1, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                System.Web.HttpContext.Current.Cache.Insert("womenNewest", womenNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            return Json(womenNewest.Skip(pageNumber - 1).Take(PageSize));
        }

        [Route("app/singleProduct")]
        public virtual ActionResult AppSingleProduct(int id)
        {
            FedtFetchProductResult product = null;
            using (var db = new DataContextDataContext())
            {
                product = db.FedtFetchProduct(id).FirstOrDefault();
            }
            if (product == null)
                return null;
            return Json(new Product { CategoryId = product.CategoryId, CategoryName = product.CategoryName, CompanyId = product.CompanyId, CompanyName = product.CompanyName, Description = product.Description, Id = product.Id,
            MainCategoryId = product.MainCategoryId, Make = product.Make, Picture = product.Picture, Price = product.Price, TheArea = (Area)product.Area, Title = product.Title });
        }

        [Route("redirect/{id}")]
        public virtual ActionResult RedirectProduct(int id)
        {
            FedtFetchProductResult product = null;
            using (var db = new DataContextDataContext())
            {
                product = db.FedtFetchProduct(id).FirstOrDefault();
            }

            return View(product);
        }

        [Route("ti-fede-shops/{name}", Order = 1)]
        public virtual ActionResult TenGreatWebshopsLink(string name)
        {
            switch (name)
            {
                case "nelly":
                    return Redirect("http://clk.tradedoubler.com/click?p(110039)a(2048951)g(17892296)");
                //case "boozt":
                //    return Redirect("https://boozt.com");
                //case "asos":
                //    return Redirect("https://asos.com");
                //case "zalando":
                //    return Redirect("https://zalando.com");
                case "bubbleroom":
                    return Redirect("https://track.adtraction.com/t/t?a=1035862042&as=1102283231&t=2&tk=1");
                case "cellbes":
                    return Redirect("https://track.adtraction.com/t/t?a=1118340539&as=1102283231&t=2&tk=1");
                //case "stylepit":
                //    return Redirect("https://Stylepit.com");
                //case "bestseller":
                //    return Redirect("http://clk.tradedoubler.com/click?p(274801)a(2048951)g(23572050)");
                case "peter-hahn":
                    return Redirect("http://tc.tradetracker.net/?c=18141&amp;m=663600&amp;a=171154&amp;r=&amp;u=");
                case "kids-world":
                    return Redirect("https://clk.tradedoubler.com/click?p=225411&a=2048951&g=22842334");

            }

            return View(MVC.Article.Views.TenGreatWebshops);
        }

        [Route("kvinde")]
        public virtual ActionResult Women()
        {
            SetGeneralValues(Utilities.Area.Women, 2, "", "");
            var womenNewest = (List<Product>)System.Web.HttpContext.Current.Cache["womenNewest"];
            if (womenNewest == null)
            {
                womenNewest = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 1000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 1000).Result;
                System.Web.HttpContext.Current.Cache.Insert("womenNewest", womenNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            ViewBag.TheTitle = "Fedt tøj til kvinder";
            ViewBag.MetaDescription = "Stort udvalg af tøj til kvinder. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
            ViewBag.MetaKeyWords = "tøj, dametøj, kvindetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
            var viewModel = new AreaViewModel { Controller = this };
            viewModel.Products = womenNewest.OrderBy(x => Guid.NewGuid()).Take(32).ToList();
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Women_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("mand")]
        public virtual ActionResult Men()
        {
            SetGeneralValues(Utilities.Area.Men, 1, "", "");
            var menNewest = (List<Product>)System.Web.HttpContext.Current.Cache["menNewest"];
            if (menNewest == null)
            {
                menNewest = Utilities.DoSearch(Utilities.Area.Men, 0, 0, 0, 1000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 1000).Result;
                System.Web.HttpContext.Current.Cache.Insert("menNewest", menNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            ViewBag.TheTitle = "Fedt tøj til mænd";
            ViewBag.MetaDescription = "Stort udvalg af tøj til mænd. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
            ViewBag.MetaKeyWords = "tøj, herretøj, mandetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
            var viewModel = new AreaViewModel{Controller = this};
            viewModel.Products = menNewest.OrderBy(x => Guid.NewGuid()).Take(32).ToList();
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Men_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("barn")]
        public virtual ActionResult Kids()
        {
            SetGeneralValues(Utilities.Area.Kids, 1, "", "");
            var kidsNewest = (List<Product>)System.Web.HttpContext.Current.Cache["kidsNewest"];
            if (kidsNewest == null)
            {
                kidsNewest = Utilities.DoSearch(Utilities.Area.Kids, 0, 0, 0, 1000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 1000).Result;
                System.Web.HttpContext.Current.Cache.Insert("kidsNewest", kidsNewest, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
            }

            ViewBag.TheTitle = "Fedt tøj til børn og unge";
            ViewBag.MetaDescription = "Stort udvalg af tøj til børn og unge. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
            ViewBag.MetaKeyWords = "tøj, børnetøj, drengetøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
            var viewModel = new AreaViewModel { Controller = this };
            viewModel.Products = kidsNewest.OrderBy(x => Guid.NewGuid()).Take(32).ToList();
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Kids_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("hurtige-laan")]
        public virtual ActionResult Loans()
        {
            return Redirect("http://lommelaan.dk");
        }

        [Route("fede-spil")]
        public virtual ActionResult Games()
        {
            return Redirect("/");
        }
        
        [Route("seprodukt/{id}", Order = 1)]
        public virtual ActionResult Goto(int id)
        {
            var product = Product.GetProduct(id, Server.MapPath("~/Indexer/LuceneIndex"));
            if (product != null)
            {
                //var referrer = Request.UrlReferrer;
                //if (referrer == null || referrer.Host.ToLower() != "fedttoj.dk")
                //{
                //    return Redirect(GetUrlHelper.GetCategoryMakeUrl(this, product.TheArea, product.MainCategoryId, product.CategoryId, product.CategoryName, product.Make));
                //}

                return Redirect(HttpUtility.UrlDecode(product.Link));
            }
            using (var db = new DataContextDataContext())
            {
                var product2 = db.FedtFetchProduct(id).FirstOrDefault();
                if (product2 != null)
                    return Redirect(HttpUtility.UrlDecode(product2.Link));                
            }
            return Redirect("/");
        }


        [Route("nedsatte-varer/{gender}", Order = 2)]
        public virtual ActionResult ReducedPrices(string gender)
        {
            var area = Utilities.GetAreaFromString(gender);
            PageSize = 20;
            SetGeneralValues(area, 3, "", "");
            var productsPercent = new List<GetBestReducedPricesPercentResult>();
            var productsKr = new List<GetBestReducedPricesKrResult>();
            using (var db = new TablesDataContext())
            {        
                productsPercent = (List<GetBestReducedPricesPercentResult>)System.Web.HttpContext.Current.Cache["ReducedPercent" + area];
                if (productsPercent == null)
                {
                    productsPercent = db.GetBestReducedPricesPercent(200, (int)area).ToList();
                    System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + area, productsPercent, null, DateTime.Now.AddHours(2), Cache.NoSlidingExpiration);
                }

                productsKr = (List<GetBestReducedPricesKrResult>)System.Web.HttpContext.Current.Cache["ReducedKr" + area];
                if (productsKr == null)
                {
                    productsKr = db.GetBestReducedPricesKr(200, (int)area).ToList();
                    System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + area, productsKr, null, DateTime.Now.AddHours(2), Cache.NoSlidingExpiration);
                }
            }
            var viewModel = new AreaViewModel
            {
                ReducedProductsPercent = productsPercent.Skip(PageSize * (PageNumber - 1)).Take(PageSize).ToList(),
                ReducedProductsKr = productsKr.Skip(PageSize * (PageNumber - 1)).Take(PageSize).ToList(),
                Controller = this,
                CurrentUrl = GetUrlHelper.GetReducedUrl(this, area),
                CurrentPageNumber = PageNumber,
                Paging = ProductHelper.SetPaging(productsPercent.Count, PageSize, PageNumber, Utilities.IsMobile())
            };

            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            switch (area)
            {
                case Utilities.Area.Men:
                    ViewBag.Title = "Nedsatte varer til mænd";
                    ViewBag.TheTitle = "Nedsatte varer af tøj til mænd - side " + PageNumber;
                    ViewBag.MetaDescription = "Nedsat tøj, udsalg. Store rabatter på tøj til mænd. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere på tilbud. Se hvor meget du kan spare";
                    ViewBag.MetaKeyWords = "rabat, nedsat, tilbud, tøj, herretøj, mandetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
                case Utilities.Area.Women:
                    ViewBag.Title = "Nedsatte varer til kvinder";
                    ViewBag.TheTitle = "Nedsatte varer af tøj til kvinder - side " + PageNumber;
                    ViewBag.MetaDescription = "Nedsat tøj, udsalg. Store rabatter på tøj til kvinder. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere på tilbud. Se hvor meget du kan spare";
                    ViewBag.MetaKeyWords = "rabat, nedsat, tilbud, tøj, dametøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
                case Utilities.Area.Kids:
                    ViewBag.Title = "Nedsatte varer til til børn og unge";
                    ViewBag.TheTitle = "Nedsatte varer af tøj til børn og unge - side " + PageNumber;
                    ViewBag.MetaDescription = "Nedsat tøj, udsalg. Store rabatter på tøj til børn og unge. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere på tilbud. Se hvor meget du kan spare";
                    ViewBag.MetaKeyWords = "rabat, nedsat, tilbud, børnetøj, drengetøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
            }
            if (PageNumber > 1)
            {
                ViewBag.PrevPage = (PageNumber - 1).ToString();
            }
            if (productsPercent.Count > PageNumber * PageSize)
            {
                ViewBag.NextPage = (PageNumber + 1).ToString();
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.ReducedPrices_Mobile, viewModel);
            return View(MVC.Home.Views.ReducedPrices, viewModel);
        }


        [Route("nyeste-toej/{gender}", Order = 2)]
        public virtual ActionResult Newest(string gender)
        {
            var area = Utilities.GetAreaFromString(gender);
        
            SetGeneralValues(area, 3, "", "");
            var products = new List<Product>();

            switch (area)
            {
                case Utilities.Area.Men:
                    products = (List<Product>)System.Web.HttpContext.Current.Cache["menNewest"];
                    if (products == null)
                    {
                        products = Utilities.DoSearch(Utilities.Area.Men, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                        System.Web.HttpContext.Current.Cache.Insert("menNewest", products, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
                    }
                    break;
                case Utilities.Area.Women:
                    products = (List<Product>)System.Web.HttpContext.Current.Cache["womenNewest"];
                    if (products == null)
                    {
                        products = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                        System.Web.HttpContext.Current.Cache.Insert("womenNewest", products, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
                    }
                    break;
                case Utilities.Area.Kids:
                    products = (List<Product>)System.Web.HttpContext.Current.Cache["kidsNewest"];
                    if (products == null)
                    {
                        products = Utilities.DoSearch(Utilities.Area.Kids, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result;
                        System.Web.HttpContext.Current.Cache.Insert("kidsNewest", products, null, DateTime.Now.AddHours(24), Cache.NoSlidingExpiration);
                    }
                    break;
            }

            var viewModel = new AreaViewModel
            {
                Products = products.Skip(PageSize * (PageNumber - 1)).Take(PageSize).ToList(),
                Controller = this,
                CurrentUrl = GetUrlHelper.GetNewestUrl(this, area),
                CurrentPageNumber = PageNumber,
                Paging = ProductHelper.SetPaging(products.Count, PageSize, PageNumber, Utilities.IsMobile())
            };
            
            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            switch (area)
            {
                case Utilities.Area.Men:
                     ViewBag.TheTitle = "Nyeste tøj til mænd - side " + PageNumber;
                    ViewBag.MetaDescription = "Nyt tøj. Stort udvalg af tøj til mænd. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
                    ViewBag.MetaKeyWords = "nyt, tøj, herretøj, mandetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
                case Utilities.Area.Women:
                    ViewBag.TheTitle = "Nyeste tøj til kvinder - side " + PageNumber;
                    ViewBag.MetaDescription = "Nyt tøj. Stort udvalg af tøj til kvinder. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
                    ViewBag.MetaKeyWords = "nyt, tøj, dametøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
                case Utilities.Area.Kids:
                    ViewBag.TheTitle = "Nyeste tøj til børn og unge - side " + PageNumber;
                    ViewBag.MetaDescription = "Nyt tøj. Stort udvalg af tøj til børn og unge. Vi har alt fra jakker til t-shirts, sko, bukser, bluser, undertøj og meget mere. Kom ind og se udvalget";
                    ViewBag.MetaKeyWords = "tøj, børnetøj, drengetøj, pigetøj, bukser, jakker, bluser, frakker, trøjer shirts, t-shirts";
                    break;
            }
            if (PageNumber > 1)
            {
                ViewBag.PrevPage = (PageNumber - 1).ToString();
            }
            if (products.Count > PageNumber * PageSize)
            {
                ViewBag.NextPage = (PageNumber + 1).ToString();
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Newest_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("{gender}/{name}", Order = 3)]
        public virtual ActionResult MainCategory(string gender, string name)
        {
            var mainCategoryId = Utilities.GetIntFromString(name);

            if (mainCategoryId == 0)
                return RedirectPermanent("/");

            var area = Utilities.GetAreaFromString(gender);

            var viewModel = GetProducts(area, mainCategoryId, 0, 0, GetQueryInt("priceFrom"), GetQueryInt("priceTo"), (SortType)GetQueryInt("sorting"));
            if (viewModel == null || !viewModel.Products.Any())
                return Redirect("/");

            viewModel = FillDescriptions(viewModel, area, mainCategoryId, 0);
            viewModel.CurrentUrl = GetUrlHelper.GetMainCategoryUrl(this, area, mainCategoryId, _mainCategories[mainCategoryId]);
            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            ViewBag.MenuArea = area;
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.MainCategory_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("{gender}/{category}/{name}")]
        public virtual ActionResult SubCategory(string gender, string category, string name)
        {
            var categoryId = Utilities.GetIntFromString(name);

            if (categoryId == 0)
                return RedirectPermanent("/");

            var area = Utilities.GetAreaFromString(gender);

            var viewModel = GetProducts(area, 0, categoryId, 0, GetQueryInt("priceFrom"), GetQueryInt("priceTo"), (SortType)GetQueryInt("sorting"));
            if (viewModel == null || !viewModel.Products.Any())
                return Redirect("/");
            viewModel = FillDescriptions(viewModel, area, viewModel.MainCategoryId, categoryId);
            viewModel.CurrentUrl = GetUrlHelper.GetCategoryUrl(this, area, category, categoryId, viewModel.Products[0].CategoryName);
            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            ViewBag.MenuArea = area;

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.MainCategory_Mobile, viewModel);
            return View(MVC.Home.Views.MainCategory, viewModel);
        }


        [Route("{gender}/{cat}/maerker/{mainId}-{categoryId}")]
        public virtual ActionResult OldMainCategoryWithMake(string gender, string cat, int mainId, int categoryId)
        {
            var makeStr = Request.QueryString["make"];
            var area = Utilities.GetAreaFromString(gender);
            FedtMake make = null;
            FedtCategory category = null;
            using (var db = new TablesDataContext())
            {
                make = db.FedtMakes.SingleOrDefault(x => x.MakeName.ToLower() == makeStr.Trim().ToLower());
                category = db.FedtCategories.SingleOrDefault(x => x.CategoryId == categoryId);
            }

            if (category == null)
                return Redirect("/");

            if (make == null)
                return RedirectPermanent(GetUrlHelper.GetMainCategoryUrl(this, area, mainId, category.CategoryName));

            return RedirectPermanent(GetUrlHelper.GetMainCategoryMakeUrl(this, area, mainId, categoryId, category.CategoryName, make.Id, make.MakeName));
        }
        
        [Route("{gender}/{cat}/maerker/{mainId}-{categoryId}-{makeId}/{makeName}")]
        public virtual ActionResult MainCategoryWithMake(string gender, string cat, int mainId, int categoryId, int makeId, string makeName)
        {
            var area = Utilities.GetAreaFromString(gender);
            var viewModel = GetProducts(area, mainId, categoryId, makeId, GetQueryInt("priceFrom"), GetQueryInt("priceTo"), (SortType)GetQueryInt("sorting"));

            if (viewModel == null || !viewModel.Products.Any())
                return Redirect("/");
            viewModel.CurrentUrl = GetUrlHelper.GetMainCategoryMakeUrl(this, area, mainId, categoryId, viewModel.Products[0].CategoryName, makeId, viewModel.Products[0].Make);
            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            viewModel = FillMakeDescriptions(viewModel, area, viewModel.MainCategoryId, viewModel.Products[0].Make);
            ViewBag.MenuArea = area;
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.MainCategory_Mobile, viewModel);
            return View(MVC.Home.Views.MainCategory, viewModel);
        }

        [Route("{gender}/{cat}/{subcat}/maerker/0-{categoryId}")]
        public virtual ActionResult OldCategoryWithMake(string gender, string cat, string subcat, int categoryId)
        {
            var makeStr = Request.QueryString["make"];
            if (makeStr == null)
                return RedirectPermanent("/");

            var area = Utilities.GetAreaFromString(gender);
            FedtMake make = null;
            FedtCategory category = null;
            using (var db = new TablesDataContext())
            {
                make = db.FedtMakes.SingleOrDefault(x => x.MakeName.ToLower() == makeStr.Trim().ToLower());
                category = db.FedtCategories.SingleOrDefault(x => x.CategoryId == categoryId);
            }

            if (category == null)
                return Redirect("/");

            if (make == null)
                return RedirectPermanent(GetUrlHelper.GetCategoryUrl(this, area, _mainCategories[category.MainCategoryId], categoryId, category.CategoryName));

           return RedirectPermanent(GetUrlHelper.GetCategoryMakeUrl(this, area, category.MainCategoryId, categoryId, category.CategoryName, make.Id, make.MakeName));
        }


        [Route("{gender}/{cat}/{subcat}/maerker/0-{categoryId}-{makeId}/{makeName}")]
        public virtual ActionResult CategoryWithMake(string gender, string cat, string subcat, int categoryId, int makeId, string makeName)
        {
            var area = Utilities.GetAreaFromString(gender);
            var viewModel = GetProducts(area, 0, categoryId, makeId, GetQueryInt("priceFrom"), GetQueryInt("priceTo"), (SortType)GetQueryInt("sorting"));

            if (viewModel == null || !viewModel.Products.Any())
                return Redirect("/");
            viewModel.CurrentUrl = GetUrlHelper.GetCategoryMakeUrl(this, area, viewModel.Products[0].MainCategoryId, categoryId, viewModel.Products[0].CategoryName, makeId, viewModel.Products[0].Make);
            ViewBag.CanonicalUrl = viewModel.CurrentUrl;
            ViewBag.RawUrl = viewModel.CurrentUrl;
            if (PageNumber > 1)
                ViewBag.CanonicalUrl = ViewBag.CanonicalUrl + "?page=" + PageNumber;
            ViewBag.MenuArea = area;
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.MainCategory_Mobile, viewModel);
            return View(MVC.Home.Views.MainCategory, viewModel);
        }

        
        private string GetTitleEnding(Utilities.Area theArea)
        {
            var ending = "";
            switch (theArea)
            {
                case Utilities.Area.Men:
                        ending = " til mænd";
                    break;
                case Utilities.Area.Women:
                        ending = " til kvinder";
                    break;
                case Utilities.Area.Kids:
                       ending = " til børn";
                    break;
                case Utilities.Area.Diverse:
                    break;
            }
            return ending;
        }


        public virtual ActionResult TopSearchModule(string searchText)
        {
            ViewBag.SearchText = searchText;
            
            return PartialView();
        }

        private void SetGeneralValues(Utilities.Area area, int mainCategoryId, string categoryName, string make)
        {
            ViewBag.ShowRobots = false;
            ViewBag.Controller = this;
            ViewBag.TheArea = area;
            ViewBag.MainCategoryName = _mainCategories[mainCategoryId];
            ViewBag.CategoryName = categoryName;
            if (Utilities.IsMobile())
                ViewBag.Make = make.LimitToLengthWithDots(13);
            else
                ViewBag.Make = make;
            ViewBag.MenuArea = Utilities.Area.All;
            var forSite = "til mænd";
            if (area == Utilities.Area.Women)
                forSite = "til kvinder";
            else if (area == Utilities.Area.Kids)
                forSite = "til børn";
            else if (area == Utilities.Area.Diverse || area == Utilities.Area.All)
                forSite = "til folk i alle aldre";
            ViewBag.ForSite = forSite;
            Response.AppendHeader("Vary", "User-Agent");
        }

        private CategoryViewModel GetProducts(Utilities.Area area, int mainCategoryId, int categoryId, int makeId, int priceFrom, int priceTo, SortType sorting)
        {
            var maxHits = PageSize* (PageNumber+11);

            ProductSearchQuery products = null;
            var cacheName = "productsA" + area + "MC" + mainCategoryId + "C" + categoryId + "M" + makeId + "p" + PageNumber;
            
            var url = HttpContext.Request.Url.AbsoluteUri;
            
            products = Utilities.DoSearch(area, makeId, mainCategoryId, categoryId, PageSize, sorting, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), priceFrom, priceTo, "", maxHits);
                        
            if (products.Result.Count < 1)
                return null;

            var make = "";
            if (makeId > 0)
                make = products.Result[0].Make;

            if (mainCategoryId < 1)
                mainCategoryId = products.Result[0].MainCategoryId;

            var viewModel = new CategoryViewModel
            {
                Products = products.Result,
                Controller = this,
                Area = area,
                CategoryId = categoryId,
                MainCategoryName = Utilities.MainCategories()[mainCategoryId],
                MainCategoryId = mainCategoryId
            };

            if (area == Utilities.Area.Women)
            {
                viewModel.AreaName = "kvinder";
                viewModel.AreaUrl = Url.Action(MVC.Home.Newest(Utilities.GetAreaString(Utilities.Area.Women)));

            }
            else if (area == Utilities.Area.Men)
            {
                viewModel.AreaName = "mænd";
                viewModel.AreaUrl = Url.Action(MVC.Home.Men());

            }
            else if (area == Utilities.Area.Kids)
            {
                viewModel.AreaName = "børn";
                viewModel.AreaUrl = Url.Action(MVC.Home.Kids());

            }
            else
            {
                viewModel.AreaName = "alle";
                viewModel.AreaUrl = Url.Action(MVC.Home.Index());
            }

            var cats = Utilities.FindCategoriesAndMakes(area, mainCategoryId, categoryId, Server.MapPath("~/Indexer/LuceneIndex"));
            ViewBag.Categories = cats.CategoryResult;
            var categoryName = "";           

            if (categoryId > 0)
            {
                var firstOrDefault = cats.CategoryResult.FirstOrDefault(x => x.CategoryId == categoryId);
                if (firstOrDefault != null)
                    categoryName = firstOrDefault.CategoryName;
            }
            SetGeneralValues(area, mainCategoryId, categoryName, make);
            ViewBag.CurrentMainCategory = mainCategoryId;
            viewModel.CategoryName = categoryName;
            viewModel.Makes = cats.MakeResult;

            var title = _mainCategories[mainCategoryId];
            if (viewModel.CategoryName != "")
                title += " - " + viewModel.CategoryName;

            if (make != "")
            {
                var urlDecode = Server.UrlDecode(make.Replace("_", "+"));
                if (urlDecode != null)
                    title += " - " + urlDecode.Replace("--", ":").Capitalize();
            }

            viewModel.Header = title;
            title += " " + GetTitleEnding(area);
         
            
            if (products.ReturnedRows > PageSize && PageNumber > 1)
                title += " - side " + PageNumber;
            ViewBag.TheTitle = title;
            
            var metaDescription = "";
            if (products.Result.Count > 0)
                metaDescription = products.Result[0].Description.Length > 10
                                             ? products.Result[0].Description
                                             : products.Result[0].Title;
            var i = 1;
            while (metaDescription.Length < 100 && products.Result.Count > i)
            {
                metaDescription += " " + (products.Result[i].Description.Length > 10 ? products.Result[i].Description : products.Result[i].Title);
                i++;
            }

            metaDescription = metaDescription.LimitToLengthWithDots(155, false);

            var metaKeyWords = "";
            if (products.Result.Count > 1)
                metaKeyWords = products.Result[1].Title;
            else if (products.Result.Count > 0)
                metaKeyWords = products.Result[0].Title;

            ViewBag.MetaDescription = metaDescription;
            ViewBag.MetaKeyWords = metaKeyWords;

            viewModel.Paging = ProductHelper.SetPaging(products.ReturnedRows, PageSize, PageNumber, Utilities.IsMobile());
            viewModel.CurrentPageNumber = PageNumber;
            
            if (PageNumber > 1)
            {
                ViewBag.PrevPage = (PageNumber - 1).ToString();
            }
            if (products.ReturnedRows > PageNumber*PageSize)
            {
                ViewBag.NextPage = (PageNumber + 1).ToString();
            }

            viewModel.PriceFrom = priceFrom;
            viewModel.PriceTo = priceTo;
            viewModel.Sorting = sorting;

            viewModel.SortTypes = new Dictionary<int, string>();
            viewModel.SortTypes.Add(0, "Nyeste først");
            viewModel.SortTypes.Add(1, "Ældste først");
            viewModel.SortTypes.Add(2, "Billigste først");
            viewModel.SortTypes.Add(3, "Dyreste først");

            if (priceTo > 0 || viewModel.Sorting != SortType.Id)
                ViewBag.ShowRobots = true;

            return viewModel;
        }


        private CategoryViewModel FillMakeDescriptions(CategoryViewModel viewModel, Utilities.Area theArea, int mainCategoryId, string make)
        {
            if (PageNumber == 1)
            { //Skal kun være på side 1 af mærket
                var url = HttpContext.Request.Url.AbsoluteUri;
                var cacheKey = "DescM" + (int)theArea + "m" + mainCategoryId + "c" + make;
                FedtFetchMakeTextsResult desc = null;
                             
                var db = new TablesDataContext();
                desc = db.FedtFetchMakeTexts((int)theArea, mainCategoryId, make).FirstOrDefault() ?? new FedtFetchMakeTextsResult();
                db.Dispose();
                             
                if (!string.IsNullOrEmpty(desc.Title))
                {
                    ViewBag.TheTitle = desc.Title;
                    // viewModel.Header = desc.Title;
                }
                if (!string.IsNullOrEmpty(desc.MetaDescription))
                    ViewBag.MetaDescription = desc.MetaDescription;
                if (!string.IsNullOrEmpty(desc.Keywords))
                    ViewBag.MetaKeyWords = desc.Keywords;
                if (!string.IsNullOrEmpty(desc.PageDescription) && desc.PageDescription.Length > 2)
                    viewModel.Description = desc.PageDescription;
                
            }
           
            return viewModel;
        }

        private CategoryViewModel FillDescriptions(CategoryViewModel viewModel, Utilities.Area theArea, int mainCategoryId, int categoryId)
        {   
            if (PageNumber < 3)
            { //Skal kun være på side 1 og 2 af kategorien
                var url = HttpContext.Request.Url.AbsoluteUri;
                var cacheKey = "DescA" + (int)theArea + "m" + mainCategoryId + "c" + categoryId + "p" + PageNumber;

                FedtFetchCategoryTextsResult desc = null;
                var db = new DataContextDataContext();
                desc = db.FedtFetchCategoryTexts((int)theArea, mainCategoryId, categoryId, PageNumber).FirstOrDefault() ?? new FedtFetchCategoryTextsResult();
               
                db.Dispose();
                
                if (!string.IsNullOrEmpty(desc.Title))
                {
                    ViewBag.TheTitle = desc.Title;
                    //viewModel.Header = desc.Title;
                }
                if (!string.IsNullOrEmpty(desc.MetaDescription))
                    ViewBag.MetaDescription = desc.MetaDescription;
                if (!string.IsNullOrEmpty(desc.Keywords))
                    ViewBag.MetaKeyWords = desc.Keywords;
                if (!string.IsNullOrEmpty(desc.PageDescription) && desc.PageDescription.Length > 2)
                    viewModel.Description = desc.PageDescription;
                                
            }            
            return viewModel;
        }

        [Route("sitemap")]
        public virtual ActionResult SiteMap()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.TheTitle = "Sitemap for fedttoj.dk";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.SiteMap_Mobile);
            return View();
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("spoergsmaal")]
        public virtual ActionResult Questions()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");

            var db = new DataContextDataContext();
            var questions = db.FedtFetchAllQuestions().ToList();
            db.Dispose();
            ViewBag.TheTitle = "Spørgsmål og svar om tøj - Fedt tøj";
            ViewBag.MetaDescription = "Spørgsmål og svar om tøj";
            ViewBag.MetaKeyWords = "Spørgsmål og svar om tøj";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Questions_Mobile, questions);
            return View(questions);
        }

        [Route("svar/{name}")]
        public virtual ActionResult Answers(string name)
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            var id = Utilities.GetIntFromString(name);

            var db = new DataContextDataContext();
            var question = db.FedtFetchQuestion(id).FirstOrDefault();
            var answers = db.FedtFetchAnswers(id).ToList();

            var viewModel = new AnswerViewModel
            {
                Answers = answers,
                Question = question
            };

            if (question != null)
            {
                ViewBag.TheTitle = question.Question + " - Fedt tøj";
                ViewBag.MetaDescription = (question.Question + " - " + ((answers.Count > 0) ? answers[0].Answer : "")).LimitToLengthWithDots(150);
                ViewBag.MetaKeyWords = question.SearchTerms;
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Answers_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("Kontakt")]
        public virtual ActionResult Contact()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.MetaDescription = "Er du i tvivl om noget på fedttoj.dk, så kontakt os";
            ViewBag.TheTitle = "Kontakt fedttoj.dk";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Contact_Mobile);
            return View();
        }
        
        [Route("om-fedttoej")]
        public virtual ActionResult About()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.MetaDescription =
                "fedttoj.dk - vi gør det lettere for dig at finde tøj i Danmark. Vi har samlet tøj fra ca. 20 forretninger, så du let kan finde det, du søger";
            ViewBag.TheTitle = "Om fedttoj.dk";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.About_Mobile);
            return View();
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("cookie-politik")]
        public virtual ActionResult Cookies()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.MetaDescription =
                "fedttoj.dk - Cookie og privatpolitik";
            ViewBag.TheTitle = "Cookie og privatpolitik";

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Cookies_Mobile);
            return View();
        }

        [Route("artikler/{url}")]
        [HttpGet]
        public virtual ActionResult ShowArticle(string url)
        {
            var article = ArticleMethods.GetArticleByUrl(url);
            if (article == null)
            {
                return Redirect("/");
            }
            SetGeneralValues(Utilities.Area.Women, 3, "Jakker", "");
            ViewBag.Title = article.Title;
            ViewBag.TheTitle = article.Title + " - fedttoj.dk";
            ViewBag.MetaDescription = article.MetaDescription;
            ViewBag.MetaKeyWords = article.MetaKeywords;
            ViewBag.PictureUrl = "https://fedttoj.dk/Content/Graphics/Articles/" + article.PictureName;
            ViewBag.ogUrl = "https://fedttoj.dk" + GetUrlHelper.GetArticleUrl(article);
            if (Utilities.IsMobile())
                return View(MVC.Article.Views.Mobile.ShowArticle_Mobile, article);
            return View(MVC.Article.Views.ShowArticle, article);
        }

        [Route("videos/{url}")]
        [HttpGet]
        public virtual ActionResult ShowVideo(string url)
        {
            var video = VideoMethods.GetVideoByUrl(url);
            if (video == null)
            {
                return Redirect("/");
            }
            SetGeneralValues(Utilities.Area.Women, 3, "Jakker", "");
            ViewBag.Title = video.Title;
            ViewBag.TheTitle = video.Title + " - fedttoj.dk";
            ViewBag.MetaDescription = video.MetaDescription;
            ViewBag.MetaKeyWords = video.MetaKeywords;
            ViewBag.PictureUrl = "https://fedttoj.dk/Content/Graphics/video/" + video.PictureName;
            ViewBag.ogUrl = "https://fedttoj.dk" + GetUrlHelper.GetVideoUrl(video);

            video.Products = Utilities.DoSearch(Utilities.Area.All, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 10000).Result.OrderBy(x => Guid.NewGuid()).Take(24).ToList();

            video.Controller = this;

            if (Utilities.IsMobile())
                return View(MVC.Article.Views.Mobile.ShowVideo_Mobile, video);
            return View(MVC.Article.Views.ShowVideo, video);
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/den-fabelagtige-skjorte", Order = 1)]
        public virtual ActionResult Shirts()
        {
            return RedirectPermanent("/artikler/den-fabelagtige-skjorte");
        }
               
        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/er-maerketoej-bedre", Order = 1)]
        public virtual ActionResult BrandClothes()
        {
            return RedirectPermanent("/artikler/er-maerketoej-bedre");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/hvad-er-toej-lavet-af", Order = 1)]
        public virtual ActionResult ClothesFabrics()
        {
            return RedirectPermanent("/artikler/hvad-er-toej-lavet-af");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/vanter-og-handsker", Order = 1)]
        public virtual ActionResult Gloves()
        {
            return RedirectPermanent("/artikler/vanter-og-handsker");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/veganertoej-oeko-toej-og-fair-trade", Order = 1)]
        public virtual ActionResult Veganer()
        {
            return RedirectPermanent("/artikler/veganertoej-oeko-toej-og-fair-trade");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/bukser-og-jeans", Order = 1)]
        public virtual ActionResult Jeans()
        {
            return RedirectPermanent("/artikler/bukser-og-jeans");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/alt-om-tshirts", Order = 1)]
        public virtual ActionResult Tshirts()
        {
            return RedirectPermanent("/artikler/alt-om-tshirts");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/trusser-de-tre-typer", Order = 1)]
        public virtual ActionResult Panties()
        {
            return RedirectPermanent("/artikler/trusser-de-tre-typer");           
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/maend-integrer-hatte-i-jeres-outfit", Order = 1)]
        public virtual ActionResult ManHats()
        {
            return RedirectPermanent("/artikler/maend-integrer-hatte-i-jeres-outfit");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/kvinder-integrer-hatte-i-jeres-outfit", Order = 1)]
        public virtual ActionResult WomenHats()
        {
            return RedirectPermanent("/artikler/kvinder-integrer-hatte-i-jeres-outfit");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/fra-basketballsko-til-popikon-historien-bag-dine-sneakers", Order = 1)]
        public virtual ActionResult Sneakers()
        {
            return RedirectPermanent("/artikler/fra-basketballsko-til-popikon-historien-bag-dine-sneakers");          
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/fra-militaeruniform-til-kontoroutfit-alt-om-slips", Order = 1)]
        public virtual ActionResult Ties()
        {
            return RedirectPermanent("/artikler/fra-militaeruniform-til-kontoroutfit-alt-om-slips");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/men-hvilken-kjole-passer-til-mig", Order = 1)]
        public virtual ActionResult Dresses()
        {
            return RedirectPermanent("/artikler/men-hvilken-kjole-passer-til-mig");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/Vinterfarver", Order = 1)]
        public virtual ActionResult WinterColors()
        {
            return RedirectPermanent("/artikler/Vinterfarver");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/10-laekre-online-toejshops", Order = 1)]
        public virtual ActionResult TenGreatWebshops()
        {
            return RedirectPermanent("/artikler/10-laekre-online-toejshops");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/coronavirus-gor-indhug-paa-tojmarkedet", Order = 1)]
        public virtual ActionResult Corona()
        {
            return RedirectPermanent("/artikler/coronavirus-gor-indhug-paa-tojmarkedet");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/guide-til-online-shopping", Order = 1)]
        public virtual ActionResult GuideOnlineShopping()
        {
            return RedirectPermanent("/artikler/guide-til-online-shopping");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/hvorfor-kobe-dansk-modetoj", Order = 1)]
        public virtual ActionResult HvorforDanskModetoej ()
        {
            return RedirectPermanent("/artikler/hvorfor-kobe-dansk-modetoj");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/Saadan-vaelger-du-det-rigtige-sommertoj", Order = 1)]
        public virtual ActionResult Sommertoej()
        {
            return RedirectPermanent("/artikler/Saadan-vaelger-du-det-rigtige-sommertoj");
        }

        [OutputCache(Duration = 604800, VaryByHeader = "User-Agent")]
        [Route("artiklerne/4-sommertrends-du-skal-holde-oje-med", Order = 1)]
        public virtual ActionResult SommerTrends()
        {
            return RedirectPermanent("/artikler/4-sommertrends-du-skal-holde-oje-med");
        }


        [Route("admin/prevsearch")]
        public virtual ActionResult SearchPages()
        {
            var viewModel = new List<string>();
            using (var db = new TablesDataContext())
            {
                var pages = db.SearchPages.OrderByDescending(x => x.SearchTerm);
                foreach (var page in pages)
                {
                    viewModel.Add("<a href=\"" + Url.Action(MVC.Home.EditPrevSearch("find", page.SearchTerm)) + "\">" + GetSearchStringFromPhrase(page.SearchTerm) + "</a>");
                }
               
            }
          
            ViewBag.TheTitle = "Tidligere søgninger";
    
           
            return View(MVC.Article.Views.SearchPages, viewModel);
        }

        [Route("admin/edit-prevsearch/{kode}/{name}")]
        [HttpGet]
        public virtual ActionResult EditPrevSearch(string kode, string name)
        {
            if (kode != "yesman")
                return Redirect("/");

            using (var db = new TablesDataContext())
            {
                var searchPage = db.SearchPages.SingleOrDefault(x => x.SearchTerm == name);
                if (searchPage == null)
                    return Redirect("/");
                return View(MVC.Home.Views.EditPrevSearch, searchPage);
            }           
        }

        [Route("admin/edit-prevsearch/{kode}/{name}")]
        [HttpPost]
        public virtual ActionResult EditPrevSearch(SearchPage page)
        {           
            using (var db = new TablesDataContext())
            {
                var searchPage = db.SearchPages.SingleOrDefault(x => x.Id == page.Id);
                if (searchPage == null)
                    return Redirect("/");

                searchPage.PageDescription = page.PageDescription;
                searchPage.Title = page.Title;
                searchPage.MetaDescription = page.MetaDescription;
                db.SubmitChanges();

                return Redirect("/admin/prevsearch");
            }
        }

        [Route("admin/edit-article/{kode}/{id}")]
        [HttpGet]
        public virtual ActionResult EditArticle(string kode, int id)
        {
            if (kode != "yesman")
                return Redirect("/");

            var article = ArticleMethods.GetArticleById(id);

            return View(MVC.Article.Views.EditArticle, article);
        }

        [Route("admin/edit-article/{kode}/{id}")]
        [HttpPost]
        public virtual ActionResult EditArticle(ArticleViewModel viewModel)
        {
            var article = ArticleMethods.SaveArticle(viewModel);

            return Redirect("/admin/edit-article/" + article.Id);
        }

        [Route("admin/edit-video/{kode}/{id}")]
        [HttpGet]
        public virtual ActionResult EditVideo(string kode, int id)
        {
            if (kode != "yesman")
                return Redirect("/");

            var article = VideoMethods.GetVideoById(id);

            return View(MVC.Article.Views.EditVideo, article);
        }

        [Route("admin/edit-video/{id}")]
        [HttpPost]
        public virtual ActionResult EditVideo(VideoViewModel viewModel)
        {
            var article = VideoMethods.SaveVideo(viewModel);

            return Redirect("/admin/edit-video/" + article.Id);
        }

        [Route("admin/generate-mobile-menu")]
        public virtual ActionResult GenerateMobileMenu()
        {
            BuildMobileMenu();
              
            return Redirect("");
        }

        [Route("artikler", Order = 1)]
        public virtual ActionResult Articles()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
          

            ViewBag.MetaDescription = "Læs artikler om tøj og mode. Du kan læse om t-shirts, jeans, bukser, skjorter, materialer og tekstiler og meget mere";
            ViewBag.MetaKeyWords = "tøj, mode, artikler, tekst, information om tøj";
           
            ViewBag.TheTitle = "Artikler om tøj og mode";

            var articles = ArticleMethods.GetAllArticles().ToList();

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Articles_Mobile, articles);
            return View(MVC.Home.Views.Articles, articles);
        }

        [Route("videoer", Order = 1)]
        public virtual ActionResult Videos()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");


            ViewBag.MetaDescription = "Se videoer om tøj og mode. Du kan se video om t-shirts, jeans, bukser, skjorter, materialer og tekstiler og meget mere";
            ViewBag.MetaKeyWords = "tøj, mode, artikler, video, information om tøj";

            ViewBag.TheTitle = "Videoer om tøj og mode";

            var videos = VideoMethods.GetAllVideos().ToList();

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Videos_Mobile, videos);
            return View(MVC.Home.Views.Videos, videos);
        }

        [Route("sitemap-kvinder", Order = 1)]
        public virtual ActionResult SitemapKvinder()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            var womenNewest = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 10000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 0).Result;
            ViewBag.Gender = "kvinder";
            ViewBag.SimemapUrl = "/sitemap-kvinder";

            var viewModel = new Dictionary<string, string>();
            foreach (var prod in womenNewest)
            {
                viewModel.Add(GetUrlHelper.GetProductUrl(this, prod), prod.Title);
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.SiteMapTop1000_Mobile, viewModel);
            return View(MVC.Home.Views.SiteMapTop1000, viewModel);
        }

        [Route("sitemap-maend", Order = 1)]
        public virtual ActionResult SitemapMaend()
        {
            SetGeneralValues(Utilities.Area.Men, 1, "", "");
            var menNewest = Utilities.DoSearch(Utilities.Area.Men, 0, 0, 0, 30000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 0).Result;
            ViewBag.Gender = "mænd";
            ViewBag.SimemapUrl = "/sitemap-maend";

            var viewModel = new Dictionary<string, string>();
            foreach (var prod in menNewest)
            {
                viewModel.Add(GetUrlHelper.GetProductUrl(this, prod), prod.Title);
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.SiteMapTop1000_Mobile, viewModel);
            return View(MVC.Home.Views.SiteMapTop1000, viewModel);
        }

        [Route("sitemap-boern", Order = 1)]
        public virtual ActionResult SitemapBoern()
        {
            SetGeneralValues(Utilities.Area.Kids, 1, "", "");
            var kidsNewest = Utilities.DoSearch(Utilities.Area.Kids, 0, 0, 0, 30000, SortType.Id, PageNumber, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0, "", 0).Result;
            ViewBag.Gender = "børn";
            ViewBag.SimemapUrl = "/sitemap-boern";

            var viewModel = new Dictionary<string, string>();
            foreach (var prod in kidsNewest)
            {
                viewModel.Add(GetUrlHelper.GetProductUrl(this, prod), prod.Title);
            }

            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.SiteMapTop1000_Mobile, viewModel);
            return View(MVC.Home.Views.SiteMapTop1000, viewModel);
        }

        [Route("rankering", Order = 1)]
        public virtual ActionResult Rankering()
        {
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.TheTitle = "Rankering af søgeresultater på fedttøj.dk";
            if (Utilities.IsMobile())
                return View(MVC.Home.Views.Mobile.Rankering_Mobile);
            return View(MVC.Home.Views.Rankering);
        }

        private MenuViewModel BuildMobileMenu()
        {
            var mainMenu = new MenuViewModel
            {
                MainCategoriesKids = new List<MenuItem>(),
                MainCategoriesWomen = new List<MenuItem>(),
                MainCategoriesMen = new List<MenuItem>(),
            };

            AddMainCategory(mainMenu, Utilities.Area.Women, 1, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 2, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 3, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 4, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 5, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 6, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Women, 7, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 8, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Women, 9, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 10, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 11, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Women, 12, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 13, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Women, 16, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Women, 18, "Alt");

            AddMainCategory(mainMenu, Utilities.Area.Kids, 1, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 2, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 3, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 4, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 5, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 6, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 7, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 8, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 9, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 10, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 11, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 12, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 13, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Kids, 14, "Alle");

            AddMainCategory(mainMenu, Utilities.Area.Men, 1, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 2, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 3, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 4, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 5, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 6, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Men, 7, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 8, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Men, 9, "Alle");
            AddMainCategory(mainMenu, Utilities.Area.Men, 11, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Men, 15, "Alt");
            AddMainCategory(mainMenu, Utilities.Area.Men, 18, "Alt");

            System.Web.HttpContext.Current.Cache.Insert("MainMenu-container", mainMenu);

            return mainMenu;
        }

        private void AddMainCategory(MenuViewModel viewModel, Area area, int mainCategoryId, string preText)
        {
            var menu = new MenuItem();
            var lucenePath = Server.MapPath("~/Indexer/LuceneIndex");

            var categories = Utilities.FindCategoriesAndMakes(area, mainCategoryId, 0, lucenePath, true);
            menu.Name = _mainCategories[mainCategoryId];
            menu.Url = GetUrlHelper.GetMainCategoryUrl(this, area, mainCategoryId, menu.Name);
            menu.CategoryId = mainCategoryId;
            menu.SubCategories = new List<MenuItem>();
            menu.PreText = preText;

            foreach (var category in categories.CategoryResult)
            {
                var subMenu = new MenuItem
                {
                    CategoryId = category.CategoryId,
                    Name = category.CategoryName,
                    Url = GetUrlHelper.GetCategoryUrl(this, area, menu.Name, category.CategoryId, category.CategoryName)
                };
                menu.SubCategories.Add(subMenu);
            }

            switch (area)
            {
                case Utilities.Area.Men:
                    viewModel.MainCategoriesMen.Add(menu);
                    break;
                case Utilities.Area.Women:
                    viewModel.MainCategoriesWomen.Add(menu);
                    break;
                case Utilities.Area.Kids:
                    viewModel.MainCategoriesKids.Add(menu);
                    break;                
            }
        }


        private string GetSearchStringFromPhrase(string search)
        {
            search = search.Replace("-", " ");
            search = search.Replace("_", " ");
            return search;
        }

    }
}
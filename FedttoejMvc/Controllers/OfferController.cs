﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Models.ViewModels;

namespace FedttoejMvc.Controllers
{
    public partial class OfferController : Controller
    {
        private readonly Dictionary<int, string> _mainCategories = Utilities.MainCategories();

       

        [Route("black-friday")]
        public virtual ActionResult CurrentOffers()
        {
            return RedirectPermanent("/");
            SetGeneralValues(Utilities.Area.Women, 13, "", "");
            ViewBag.MetaDescription =
                "Black friday - få store rabatter på tøj og sko på Black friday";
            ViewBag.TheTitle = "Black friday - få store rabatter på tøj og sko på Black friday";
            var womenNewest = (List<Product>)System.Web.HttpContext.Current.Cache["womenNewest"];
            if (womenNewest == null)
            {
                womenNewest = Utilities.DoSearch(Utilities.Area.Women, 0, 0, 0, 1000, SortType.Id, 1, Server.MapPath("~/Indexer/LuceneIndex"), 0, 0).Result;
                System.Web.HttpContext.Current.Cache.Insert("womenNewest", womenNewest, null, DateTime.Now.AddHours(20), Cache.NoSlidingExpiration);
            }
            var viewModel = new AreaViewModel
            {
                Controller = this,
                Products = womenNewest.OrderBy(x => Guid.NewGuid()).Take(8).ToList()
            };

            if (Utilities.IsMobile())
                return View(MVC.Offer.Views.CurrentOffers_Mobile, viewModel);
            return View(MVC.Offer.Views.CurrentOffers, viewModel);
        }


        [Route("konkurrence")]
        public virtual ActionResult WritingContest()
        {
            return RedirectToActionPermanent(MVC.Home.Index());

            ViewBag.showCatfish = false;
            SetGeneralValues(Utilities.Area.Women, 13, "", "");

            if (Utilities.IsMobile())
                return View(MVC.Offer.Views.WritingContest_Mobile);
            return View();
        }

        [HttpPost, Route("konkurrence")]
        public virtual ActionResult WritingContest(ContestViewModel viewModel)
        {
            return RedirectToActionPermanent(MVC.Home.Index());

            ViewBag.showCatfish = false;
            using (var db = new TablesDataContext())
            {
                db.FedtSaveWritingContest(viewModel.Name, viewModel.Email, viewModel.Story);
            }

            SetGeneralValues(Utilities.Area.Women, 13, "", "");

            ViewBag.ThanksContest = "1";
            if (Utilities.IsMobile())
                return View(MVC.Offer.Views.WritingContest_Mobile, viewModel);
            return View();
        }

        private void SetGeneralValues(Utilities.Area area, int mainCategoryId, string categoryName, string make)
        {
            ViewBag.Controller = this;
            ViewBag.TheArea = area;
            ViewBag.MainCategoryName = _mainCategories[mainCategoryId];
            ViewBag.CategoryName = categoryName;
            if (Utilities.IsMobile())
                ViewBag.Make = make.LimitToLengthWithDots(13);
            else
                ViewBag.Make = make;
            ViewBag.MenuArea = Utilities.Area.All;
            var forSite = "til mænd";
            if (area == Utilities.Area.Women)
                forSite = "til kvinder";
            else if (area == Utilities.Area.Kids)
                forSite = "til børn";
            else if (area == Utilities.Area.Diverse || area == Utilities.Area.All)
                forSite = "til folk i alle aldre";
            ViewBag.ForSite = forSite;
            Response.AppendHeader("Vary", "User-Agent");
        }

    }
}
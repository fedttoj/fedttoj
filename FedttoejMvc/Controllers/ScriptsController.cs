﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.UI;
using Lucene.Net.Analysis;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Store;
using Directory = Lucene.Net.Store.Directory;
using System.Web.Mvc;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Helpers;
using System.Web.Caching;

namespace FedttoejMvc.Controllers
{
    public partial class ScriptsController : Controller
    {
        private readonly Dictionary<int, string> _mainCategories = Utilities.MainCategories();

        // GET: Scripts
        [Route("indexnow.aspx")]
        public virtual ActionResult Indexer()
        {
            Server.ScriptTimeout = 500;
            FillProducts();
            return View();
        }

        public void FillProducts()
        {

            Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
            Directory index = FSDirectory.Open(new DirectoryInfo(Server.MapPath("~/Indexer/LuceneIndex")));
            var writer = new IndexWriter(index, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED);

            const int startIndex = int.MaxValue;
            const int count = 20000;
            var db = new DataContextDataContext();
            var products = db.FedtFetchActiveProductsbyId(startIndex, count).ToList();
            while (products.Any())
            {
                var lastOne = AddToIndex(products, writer);

                products = db.FedtFetchActiveProductsbyId(lastOne, count).ToList();
            }
            writer.Optimize();
            writer.Commit();
            writer.Close();
            analyzer.Close();
            index.Close();
            db.Dispose();
        }

        private int AddToIndex(IEnumerable<FedtFetchActiveProductsbyIdResult> products, IndexWriter writer)
        {
            var lastOne = 0;
            foreach (var product in products)
            {
                product.Title = product.Title.Replace("%", "").Replace("<", "").Replace("^", "");
                var doc = new Document();
                doc.Add(new Field("Id", product.Id.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new NumericField("IdInt", Field.Store.NO, true).SetIntValue(product.Id));
                doc.Add(new NumericField("CreatedInt", Field.Store.NO, true).SetIntValue(GetIntFromYear(product.CreatedDate)));
                doc.Add(new Field("MainCategoryId", product.MainCategoryId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("MainCategoryName", _mainCategories[product.MainCategoryId], Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("CategoryId", product.CategoryId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("CategoryName", product.CategoryName, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Link", product.Link, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Title", product.Title, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Make", product.Make, Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("Description", Utilities.ReplaceDesciptionText(product.Description), Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Picture", product.Picture, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Status", product.Status.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("Price", product.Price.ToString("n2"), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new NumericField("PriceSort", Field.Store.NO, true).SetIntValue((int)(product.Price * 100)));
                doc.Add(new Field("CompanyId", product.CompanyId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("MakeId", product.MakeId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                doc.Add(new Field("CompanyName", product.CompanyName, Field.Store.YES, Field.Index.NO));
                doc.Add(new Field("Area", product.Area.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));

                var searchBody = product.CategoryName + " " + _mainCategories[product.MainCategoryId] + " " + product.Title.Replace("-", " ") + " " + product.Description.Replace("-", " ") + " " + product.CompanyName;

                switch ((Utilities.Area)product.Area)
                {
                    case Utilities.Area.Men:
                        searchBody += " mand mænd herre herretøj mandetøj";
                        break;
                    case Utilities.Area.Women:
                        searchBody += " kvinde kvinder dame dametøj kvindetøj damer";
                        break;
                    case Utilities.Area.Kids:
                        searchBody += " dreng drenge drengetøj pige piger pigetøj ungdomstøj børn barn unge ung ";
                        break;
                }

                searchBody += " for til tøj kluns smart fint flot ";
                doc.Add(new Field("SearchBody", searchBody, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
                writer.AddDocument(doc);
                lastOne = product.Id;

            }
            return lastOne;
        }

        private static int GetIntFromYear(DateTime createdDate)
        {
            var result = (createdDate.Year - 2010).ToString();
            if (createdDate.Month < 10)
                result += "0" + createdDate.Month;
            else
                result += createdDate.Month;
            if (createdDate.Day < 10)
                result += "0" + createdDate.Day;
            else
                result += createdDate.Day;

            return int.Parse(result);
        }

        [Route("categoryindexnow.aspx")]
        public virtual ActionResult CategoryIndexer()
        {
            Server.ScriptTimeout = 500;
            FillCategoryProducts();
            return View();
        }

        public void FillCategoryProducts()
        {
            
            Analyzer analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_29);
            Directory index = FSDirectory.Open(new DirectoryInfo(Server.MapPath("~/Indexer/CategoryIndex")));
            var writer = new IndexWriter(index, analyzer, true, IndexWriter.MaxFieldLength.UNLIMITED);

            const int startIndex = int.MaxValue;
            const int count = 20000;
            var db = new DataContextDataContext();
            var products = db.FedtFetchActiveProductsbyId(startIndex, count).ToList();
            while (products.Count() > 0)
            {
                var lastOne = AddToCategoryIndex(products, writer);
                products = db.FedtFetchActiveProductsbyId(lastOne, count).ToList();
            }
            writer.Optimize();
            writer.Commit();
            writer.Close();
            analyzer.Close();
            index.Close();
            db.Dispose();
            
        }

        private int AddToCategoryIndex(IEnumerable<FedtFetchActiveProductsbyIdResult> products, IndexWriter writer)
        {
            var lastOne = 0;

            foreach (var product in products)
            {
                if (product.Status == 1)
                {
                    var doc = new Document();
                    doc.Add(new Field("MainCategoryId", product.MainCategoryId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                    doc.Add(new Field("MainCategoryName", _mainCategories[product.MainCategoryId], Field.Store.YES, Field.Index.NO));
                    doc.Add(new Field("CategoryId", product.CategoryId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                    doc.Add(new Field("CategoryName", product.CategoryName, Field.Store.YES, Field.Index.NO));
                    doc.Add(new Field("Make", product.Make, Field.Store.YES, Field.Index.NO));
                    doc.Add(new Field("MakeId", product.MakeId.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));
                    doc.Add(new Field("Area", product.Area.ToString(), Field.Store.YES, Field.Index.NOT_ANALYZED_NO_NORMS));

                    var searchBody = product.CategoryName + " " + _mainCategories[product.MainCategoryId] + " " + product.Title.Replace("-", " ") + " " + product.Description.Replace("-", " ") + " " + product.CompanyName;

                    switch ((Utilities.Area)product.Area)
                    {
                        case Utilities.Area.Men:
                            searchBody += " mand mænd herre herretøj mandetøj";
                            break;
                        case Utilities.Area.Women:
                            searchBody += " kvinde kvinder dame dametøj kvindetøj damer";
                            break;
                        case Utilities.Area.Kids:
                            searchBody += " dreng drenge drengetøj pige piger pigetøj ungdomstøj børn barn unge ung ";
                            break;

                    }

                    searchBody += " for til tøj kluns smart fint flot " + product.CompanyName;
                    doc.Add(new Field("SearchBody", searchBody, Field.Store.NO, Field.Index.ANALYZED_NO_NORMS));
                    writer.AddDocument(doc);
                    lastOne = product.Id;
                }
            }
            return lastOne;
        }

        [Route("searchsitemap.aspx")]
        public virtual ActionResult GenerateSearchSitemap()
        {
            Server.ScriptTimeout = 500;
            var file = new StreamWriter(Server.MapPath("~/scripts/searchSitemap1.xml"));
            file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            file.WriteLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");
            file.WriteLine("<url>");
            file.WriteLine("<loc>https://fedttoj.dk/tidligere-sogninger</loc>");
            file.WriteLine("</url>");
            var db = new TablesDataContext();
            var searchPages = db.SearchPages.ToList();
            foreach (var page in searchPages)
            {
                if (page.SearchTerm == "gucci tasker")
                    continue;

                file.WriteLine("<url>");
                file.WriteLine("<loc>https://fedttoj.dk/s/" + page.SearchTerm + "</loc>");
                if (page.SearchTerm == "gucci taske" || !string.IsNullOrWhiteSpace(page.PageDescription))
                    file.WriteLine("<priority>1.0</priority>");
                else
                    file.WriteLine("<priority>0.6</priority>");
                file.WriteLine("</url>");
            }
            file.WriteLine("</urlset>");
            file.Close();

            db.Dispose();
            return View(MVC.Scripts.Views.GenerateGoogleSitemap);
        }

        [Route("generatesitemapnow.aspx")]
        public virtual ActionResult GenerateGoogleSitemap()
        {
            Server.ScriptTimeout = 500;
            var startIdex = int.MaxValue;
            var count = 20000;
            var db = new DataContextDataContext();
            var products = db.FedtFetchActiveProductsbyId(startIdex, count).ToList();
            var counter = 1;
            var lastOne = 0;

            var file = new StreamWriter(Server.MapPath("~/sitemapNew.xml"));
            file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            file.WriteLine("<sitemapindex xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">");

            while (products.Count() > 0)
            {
                lastOne = FillProductSiteMap(products, counter);

                products = db.FedtFetchActiveProductsbyId(lastOne, count).ToList();

                if (counter > 30)
                {
                    file.WriteLine("<sitemap>");
                    file.WriteLine("<loc>https://fedttoj.dk/scripts/googleSiteProductsNew" + counter + ".xml</loc>");
                    file.WriteLine("<lastmod>" + DateTime.Now.Year + "-" + FormatDate(DateTime.Now.Month) + "-" + FormatDate(DateTime.Now.Day) + "T" + FormatDate(DateTime.Now.Hour) + ":" + FormatDate(DateTime.Now.Minute) + ":" + FormatDate(DateTime.Now.Second) + "+01:00</lastmod>");
                    file.WriteLine(" </sitemap>");
                }
                counter++;
            }
            db.Dispose();

            file.WriteLine("</sitemapindex>");
            file.Close();

            FillCategoryUrls();
            return View();
        }


        private void FillCategoryUrls()
        {
            var file = new StreamWriter(Server.MapPath("~/scripts/categorySitemap.xml"));
            file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            file.WriteLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");

            AddUrl(file, "/kvinde", "1.0");
            AddUrl(file, "/mand", "1.0");
            AddUrl(file, "/barn", "1.0");
            AddMainCategory(file, Utilities.Area.Women, 1);
            AddMainCategory(file, Utilities.Area.Women, 2);
            AddMainCategory(file, Utilities.Area.Women, 3);
            AddMainCategory(file, Utilities.Area.Women, 4);
            AddMainCategory(file, Utilities.Area.Women, 5);
            AddMainCategory(file, Utilities.Area.Women, 6);
            AddMainCategory(file, Utilities.Area.Women, 7);
            AddMainCategory(file, Utilities.Area.Women, 8);
            AddMainCategory(file, Utilities.Area.Women, 9);
            AddMainCategory(file, Utilities.Area.Women, 10);
            AddMainCategory(file, Utilities.Area.Women, 11);
            AddMainCategory(file, Utilities.Area.Women, 12);
            AddMainCategory(file, Utilities.Area.Women, 13);
            AddMainCategory(file, Utilities.Area.Women, 16);

            AddMainCategory(file, Utilities.Area.Kids, 1);
            AddMainCategory(file, Utilities.Area.Kids, 2);
            AddMainCategory(file, Utilities.Area.Kids, 3);
            AddMainCategory(file, Utilities.Area.Kids, 4);
            AddMainCategory(file, Utilities.Area.Kids, 5);
            AddMainCategory(file, Utilities.Area.Kids, 6);
            AddMainCategory(file, Utilities.Area.Kids, 7);
            AddMainCategory(file, Utilities.Area.Kids, 8);
            AddMainCategory(file, Utilities.Area.Kids, 9);
            AddMainCategory(file, Utilities.Area.Kids, 10);
            AddMainCategory(file, Utilities.Area.Kids, 11);
            AddMainCategory(file, Utilities.Area.Kids, 12);
            AddMainCategory(file, Utilities.Area.Kids, 13);
            AddMainCategory(file, Utilities.Area.Kids, 14);

            AddMainCategory(file, Utilities.Area.Men, 1);
            AddMainCategory(file, Utilities.Area.Men, 2);
            AddMainCategory(file, Utilities.Area.Men, 3);
            AddMainCategory(file, Utilities.Area.Men, 4);
            AddMainCategory(file, Utilities.Area.Men, 5);
            AddMainCategory(file, Utilities.Area.Men, 6);
            AddMainCategory(file, Utilities.Area.Men, 7);
            AddMainCategory(file, Utilities.Area.Men, 8);
            AddMainCategory(file, Utilities.Area.Men, 9);
            AddMainCategory(file, Utilities.Area.Men, 11);
            AddMainCategory(file, Utilities.Area.Men, 15);
            AddMainCategory(file, Utilities.Area.Men, 18);

            AddMainCategory(file, Utilities.Area.Diverse, 1);
            AddMainCategory(file, Utilities.Area.Diverse, 2);
            AddMainCategory(file, Utilities.Area.Diverse, 3);
            AddMainCategory(file, Utilities.Area.Diverse, 4);
            AddMainCategory(file, Utilities.Area.Diverse, 5);
            AddMainCategory(file, Utilities.Area.Diverse, 6);
            AddMainCategory(file, Utilities.Area.Diverse, 7);
            AddMainCategory(file, Utilities.Area.Diverse, 8);
            AddMainCategory(file, Utilities.Area.Diverse, 9);
            AddMainCategory(file, Utilities.Area.Diverse, 11);

            AddArticles(file);
            AddVideos(file);
            AddFAQ(file);
            
            file.WriteLine("</urlset>");
            file.Close();
           
        }

        private void AddFAQ(StreamWriter file)
        {
            AddUrl(file, "/svar/9-Er-det-sikkert-at-koebe-toej-fra-en-online-toej-shop", "0.5");
            AddUrl(file, "/svar/18-Hvad-kan-jeg-goere-hvis-toejet-ikke-passer", "0.5");
            AddUrl(file, "/svar/3-Hvor-er-det-bedst-at-koebe-toej-online", "0.5");
            AddUrl(file, "/svar/2-Hvor-finder-jeg-en-solid-flyverdragt-til-et-barn-paa-3-aar", "0.5");
            AddUrl(file, "/svar/6-Hvor-finder-jeg-noget-billigt-herretoej-i-god-kvalitet", "0.5");
            AddUrl(file, "/svar/13-Hvor-finder-jeg-noget-fedt-toej-fra-Day-Birger-Mikkelsen", "0.5");
            AddUrl(file, "/svar/1-Hvor-finder-man-en-fed-festkjole", "0.5");
            AddUrl(file, "/svar/11-Hvor-kan-man-koebe-Hunter-gummistoevler-De-er-bare-fede", "0.5");
            AddUrl(file, "/svar/16-Kan-man-regne-med-de-stoerrelser-som-online-butikker-viser-paa-feks-bukser-bluser-og-t-shirts", "0.5");
            AddUrl(file, "/svar/15-Ved-I-hvor-man-kan-faa-en-fed-jakke-til-billige-penge", "0.5");
            AddUrl(file, "/nedsatte-varer/mand", "1.0");
            AddUrl(file, "/nedsatte-varer/kvinde", "1.0");

        }

        private void AddMainCategory(StreamWriter file, Utilities.Area area, int mainCategoryId)
        {
            var mainCategoryName = _mainCategories[mainCategoryId];

            var mainLink = GetUrlHelper.GetMainCategoryUrl(this, area, mainCategoryId, mainCategoryName);
            AddUrl(file, mainLink, "1.0");

            var db = new TablesDataContext();
            var categories = db.FedtCategories.Where(x => x.MainCategoryId == mainCategoryId).ToList();
            foreach (var category in categories)
            {               
                var link = GetUrlHelper.GetCategoryUrl(this, area, mainCategoryName, category.CategoryId, category.CategoryName);
                AddUrl(file, link, "1.0");             
            }

            db.Dispose();
        }

        private void AddArticles(StreamWriter file)
        {
            AddUrl(file, "/artikler", "0.5");
            var articles = ArticleMethods.GetAllArticles();
                        
            foreach (var article in articles)
            {
                var link = GetUrlHelper.GetArticleUrl(article);
                AddUrl(file, link, "1.0");
            }
        }

        private void AddVideos(StreamWriter file)
        {
            AddUrl(file, "/videoer", "0.5");
            var videos = VideoMethods.GetAllVideos();

            foreach (var video in videos)
            {
                var link = GetUrlHelper.GetVideoUrl(video);
                AddUrl(file, link, "0.5");
            }
        }

        private void AddUrl(StreamWriter file, string link, string priority)
        {
            file.WriteLine("<url>");
            file.WriteLine("<loc>https://fedttoj.dk" + link + "</loc>");
            file.WriteLine("<lastmod>" + DateTime.Now.Year + "-" + FormatDate(DateTime.Now.Month) + "-" + FormatDate(DateTime.Now.Day) + "T" + FormatDate(DateTime.Now.Hour) + ":" + FormatDate(DateTime.Now.Minute) + ":" + FormatDate(DateTime.Now.Second) + "+01:00</lastmod>");
            file.WriteLine("<changefreq>daily</changefreq>");
            file.WriteLine("<priority>" + priority + "</priority>");
            file.WriteLine("</url>");
        }


        private int FillProductSiteMap(List<FedtFetchActiveProductsbyIdResult> products, int counter)
        {
            var file = new StreamWriter(Server.MapPath("~/scripts/googleSiteProductsNew" + counter + ".xml"));
            file.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
            file.WriteLine("<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");
            var lastOne = 0;

            foreach (var product in products)
            {
                if (product.Status > 0)
                {
                    product.Title = product.Title.Replace("%", "").Replace("<", "").Replace("^", "");
                    var prod = new Product {CategoryId = product.CategoryId, CategoryName = product.CategoryName, Id = product.Id, TheArea = (Utilities.Area)product.Area, Title = product.Title, MainCategoryId = product.MainCategoryId};
                    var link = GetUrlHelper.GetProductUrl(this, prod);
                    file.WriteLine("<url>");
                    file.WriteLine("<loc>https://fedttoj.dk" + link + "</loc>");
                    file.WriteLine("<lastmod>" + product.EditedDate.Year + "-" + FormatDate(product.EditedDate.Month) + "-" + FormatDate(product.EditedDate.Day) + "T" + FormatDate(product.EditedDate.Hour) + ":" + FormatDate(product.EditedDate.Minute) + ":" + FormatDate(product.EditedDate.Second) + "+01:00</lastmod>");
                    file.WriteLine("<changefreq>weekly</changefreq>");
                    file.WriteLine("<priority>0.7</priority>");
                    file.WriteLine("</url>");
                    lastOne = product.Id;
                }
            }

            file.WriteLine("</urlset>");
            file.Close();
            return lastOne;
        }

        private string FormatDate(int number)
        {
            var numberStr = number.ToString();
            if (numberStr.Length == 1)
                numberStr = "0" + numberStr;

            return numberStr;

        }


        [Route("WarmCacheKids.aspx")]
        public virtual ActionResult WarmCacheKids()
        {
            Server.ScriptTimeout = 5000;
            WarmCategoriesAndMakes(Utilities.Area.Kids);
            WarmCategoriesAndMakes(Utilities.Area.Diverse);
            return View("WarmCache");
        }

        [Route("WarmCacheWomen.aspx")]
        public virtual ActionResult WarmCacheWomen()
        {
            Server.ScriptTimeout = 5000;
            WarmCategoriesAndMakes(Utilities.Area.Women);
            return View("WarmCache");
        }

        [Route("WarmCacheMen.aspx")]
        public virtual ActionResult WarmCacheMen()
        {
            Server.ScriptTimeout = 5000;
            WarmCategoriesAndMakes(Utilities.Area.Men);
            return View("WarmCache");
        }


        private void WarmCategoriesAndMakes(Utilities.Area area)
        {
            var lucenePath = Server.MapPath("~/Indexer/LuceneIndex");
            foreach (var mainCategory in _mainCategories)
            {
                Utilities.FindCategoriesAndMakes(area, mainCategory.Key, 0, lucenePath, true);

                using (var db = new DataContextDataContext())
                {
                    var categories = db.FedtFetchCategoryByMainId(mainCategory.Key, (int) area);
                    foreach (var category in categories)
                    {
                        Utilities.FindCategoriesAndMakes(area, 0, category.CategoryId, lucenePath, true);
                    }
                }
            }
        }

        [Route("PopulateCache.aspx")]
        public virtual ActionResult PopulateCache()
        {
            using (var db = new TablesDataContext())
            {
                var productsPercentWoman = db.GetBestReducedPricesPercent(500, (int)Utilities.Area.Women).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Women, productsPercentWoman, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);

                var productsKrWoman = db.GetBestReducedPricesKr(500, (int)Utilities.Area.Women).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Women, productsKrWoman, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);

                var productsPercentMan = db.GetBestReducedPricesPercent(500, (int)Utilities.Area.Men).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Men, productsPercentMan, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);

                var productsKrMan = db.GetBestReducedPricesKr(500, (int)Utilities.Area.Men).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Men, productsKrMan, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);

                var productsPercentKids = db.GetBestReducedPricesPercent(500, (int)Utilities.Area.Kids).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedPercent" + Utilities.Area.Kids, productsPercentKids, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);

                var productsKrKids = db.GetBestReducedPricesKr(500, (int)Utilities.Area.Kids).ToList();
                System.Web.HttpContext.Current.Cache.Insert("ReducedKr" + Utilities.Area.Kids, productsKrKids, null, DateTime.Now.AddHours(4), Cache.NoSlidingExpiration);
            }
            return View("CalculatePriceReduced");
        }

        [Route("CalculatePriceReduced.aspx")]
        public virtual ActionResult CalculatePriceReduced()
        {
            Server.ScriptTimeout = 6000;
            
            var results = new List<PriceReduction>();
            using (var db = new TablesDataContext())
            {                
                db.ExecuteCommand("truncate table PriceReductions");
                var counter = 0;
                var prices = db.GetHistoryChanges();
                foreach (var price in prices)
                {
                    var productId = price.productid;
                    if (results.Any(x => x.ProductId == productId))
                        continue;

                    var pricesForProduct = db.PriceHistories.Where(x => x.ProductId == productId).OrderByDescending(x => x.TimeStamp).Take(2).ToList();
                    
                    if (pricesForProduct.Count() < 2)
                        continue;
                    if (pricesForProduct[0].Price > pricesForProduct[1].Price)
                        continue;

                    var reductionKr = (int) (pricesForProduct[1].Price - pricesForProduct[0].Price);
                    var reductionPercent = (int)(reductionKr * 100 / pricesForProduct[1].Price);
                    
                    var priceReduction = new PriceReduction
                    {
                        ProductId = productId,
                        BeforePrice = pricesForProduct[1].Price,
                        NowPrice = pricesForProduct[0].Price,
                        TimeStamp = pricesForProduct[0].TimeStamp,
                        ReductionKr = reductionKr,
                        ReductionPercent = reductionPercent
                    };

                    results.Add(priceReduction);
                    db.PriceReductions.InsertOnSubmit(priceReduction);
                    counter++;
                    if (counter == 20)
                    {
                        db.SubmitChanges();
                        counter = 0;
                    }
                }
               
            }

            return View("CalculatePriceReduced");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;
using FedttoejMvc.Models.ViewModels;

namespace FedttoejMvc.Controllers
{
    public partial class SearchController : Controller
    {
        private const int PageSize = 36;
        private readonly Dictionary<int, string> _mainCategories = Utilities.MainCategories();
        private int PageNumber
        {
            get { return !string.IsNullOrEmpty(Request.QueryString["Page"]) ? int.Parse(Request.QueryString["Page"]) : 1; }
        }

        private string SearchText
        {
            get { return Request.QueryString["SearchString"]; }

        }

        //public Utilities.Area TheArea
        //{
        //    get
        //    {
        //        return !string.IsNullOrEmpty(Request.QueryString["Area"])
        //                   ? (Utilities.Area)int.Parse(Request.QueryString["Area"])
        //                   : Utilities.Area.Women;
        //    }
        //}

        // GET: Search
        [Route("soeg")]
        public virtual ActionResult SearchResult()
        {           
            var searchPhrase = Utilities.GetSearchPhrase(SearchText);
            var url = Url.Action(MVC.Home.Search(searchPhrase)) + Utilities.GetSearchQuery(Utilities.GetQueryInt("priceFrom"), Utilities.GetQueryInt("priceTo"), (SortType)Utilities.GetQueryInt("sorting"));
            return Redirect(url);
        }
        
    }
}
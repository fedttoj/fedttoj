﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using FedttoejMvc.Helpers;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Models.ViewModels;
using System;
using static FedttoejMvc.Models.Utilities;

namespace FedttoejMvc.Controllers
{
    public partial class ShowController : Controller
    {
        private readonly Dictionary<int, string> _mainCategories = Utilities.MainCategories();

        [Route("{gender}/produkt/{cat}/{subcat}/{name}")]
        public virtual ActionResult Show(string gender, string cat, string subcat, string name)
        {
            var id = Utilities.GetIntFromString(name);
            FedtFetchProductResult product = null; // = Product.GetProduct(id, Server.MapPath("~/Indexer/LuceneIndex"));
           
            using (var db = new DataContextDataContext())
            {
                product = db.FedtFetchProduct(id).FirstOrDefault(); 
            }
            
            if (product == null || product.Id == 0)
            {
                var url = HttpContext.Request.RawUrl.ToLower();
                if (url.Contains("mand"))
                    return RedirectPermanent("/mand");
                if (url.Contains("kvinde"))
                    return RedirectPermanent("/nyeste-toej/kvinde");
                if (url.Contains("barn"))
                    return RedirectPermanent("/barn");
                if (url.Contains("diverse"))
                    return RedirectPermanent("/diverse/6-Diverse");
                return RedirectPermanent("/");
                    
            }
           
            var redirectUrl = CheckForRedirect(product);
            if (redirectUrl != "")
            {
                return RedirectPermanent(redirectUrl);
            }
            var products = DoSearch(product).Result.Where(x => x.Id != product.Id).Take(12).ToList();
            var viewModel = new ShowViewModel
            {
                Product = product,
                CategoryName = product.CategoryName,
                MainCategoryName = _mainCategories[product.MainCategoryId],
                CategoryUrl = GetUrlHelper.GetCategoryUrl(this, (Area)product.Area, _mainCategories[product.MainCategoryId], product.CategoryId, product.CategoryName),
                MainCategoryUrl = GetUrlHelper.GetMainCategoryUrl(this, (Area)product.Area, product.MainCategoryId, _mainCategories[product.MainCategoryId]),
                MakeUrl = ""
            };

            if ((Area)product.Area == Utilities.Area.Women)
            {
                viewModel.AreaName = "kvinder";
                viewModel.AreaUrl = Url.Action(MVC.Home.Newest(Utilities.GetAreaString(Utilities.Area.Women)));
                 
            }
            else if ((Area)product.Area == Utilities.Area.Men)
            {
                viewModel.AreaName = "mænd";
                viewModel.AreaUrl = Url.Action(MVC.Home.Men());
                 
            }
            else if ((Area)product.Area == Utilities.Area.Kids)
            {
                viewModel.AreaName = "børn";
                viewModel.AreaUrl = Url.Action(MVC.Home.Kids());
                 
            }
            else 
            {
                viewModel.AreaName = "alle";
                viewModel.AreaUrl = Url.Action(MVC.Home.Index());
            }
         
            viewModel.Products = products;
            if (product.Make != null)
                viewModel.MakeUrl = GetUrlHelper.GetCategoryMakeUrl(this, (Area)product.Area, product.MainCategoryId, product.CategoryId, product.CategoryName, product.MakeId.GetValueOrDefault(), product.Make);
            
            viewModel.Controller = this;

            if (product.CompanyId == 1 || product.CompanyId == 34 || product.CompanyId == 4 || product.CompanyId == 21 
            || product.CompanyId == 10 || product.CompanyId == 25 || product.CompanyId == 32 || product.CompanyId == 40)
               viewModel.FreePostage = true;

            viewModel.PinterestUrl = Server.UrlEncode("https://fedttoj.dk" + GetUrlHelper.GetProductUrl(this, product));
            viewModel.PinterestTitle = Server.UrlEncode(product.Title);
            viewModel.PinterestImage = Server.UrlEncode(GetUrlHelper.GetPictureUrl(product.CompanyId, product.Picture, product.Id, product.Title));
            
            viewModel = FillNavigation(viewModel);

            ViewBag.CanonicalUrl = GetUrlHelper.GetProductUrl(this, product);
            ViewBag.ogUrl = "https://fedttoj.dk" + GetUrlHelper.GetProductUrl(this, product);

            Response.AppendHeader("Vary", "User-Agent");
            if (Utilities.IsMobile())
                return View(MVC.Show.Views.Show_Mobile, viewModel);
            return View(viewModel);
        }

        [Route("data/produkt/pricehistory/{id}")]
        public virtual JsonResult PriceHistory(int id)
        {
            Array[] OuterResult = new Array[0];
            using (var db = new TablesDataContext())
            {
                var history = db.PriceHistories.Where(x => x.ProductId == id).OrderBy(x => x.TimeStamp).ToList();
                if (history.Count > 0)
                {
                    var startDate = history[0].TimeStamp;
                    var endDate = DateTime.Now;
                    var dateDiff = endDate - startDate;
                    Array[] result = new Array[dateDiff.Days + 1];

                    var counter = 0;
                    var lastPrice = history[0].Price;
                    while(startDate <= endDate)
                    {
                        var hist = history.SingleOrDefault(x => x.TimeStamp.Year == startDate.Year && x.TimeStamp.Month == startDate.Month && x.TimeStamp.Day == startDate.Day);
                        if (hist != null)
                        {
                            result[counter] = new object[2] { startDate.Day + "/" + startDate.Month, (int)hist.Price };
                            lastPrice = hist.Price;
                        }
                        else
                            result[counter] = new object[2] { startDate.Day + "/" + startDate.Month, (int)lastPrice };

                        counter++;
                        startDate = startDate.AddDays(1);
                    }
                    OuterResult = result;
                }
            }

            return Json(OuterResult, JsonRequestBehavior.AllowGet);
        }

      
        //private void LogIdAndMake(string make, int id, int mainCategoryId)
        //{
           
        //}

        private string CheckForRedirect(FedtFetchProductResult product)
        {
            if (product.Status == 1)
            {
                var link = GetUrlHelper.GetProductUrl(this, product);
                if (!Request.RawUrl.Contains(link))
                {
                    return link;
                }
            }

            var products = new List<Product>();

            if (product.Status == 0)
            {
                products = DoSearch(product).Result;
            }

            if (product.Status == 0 && products.Count > 0)
            {
                var i = 0;

                var product1 = products[i];
                using (var db = new DataContextDataContext())
                {
                    while (product.Status == 0 && i < products.Count)
                    {
                        product = db.FedtFetchProduct(product1.Id).FirstOrDefault();
                        i++;
                        if (i < products.Count)
                            product1 = products[i];
                    }
                }
                if (product.Status > 0)
                {
                    var url = GetUrlHelper.GetProductUrl(this, product);
                    return url;
                }
            }
            if (product.Status == 0)
            {
                var url = HttpContext.Request.RawUrl.ToLower();
                if (url.Contains("mand"))
                    return "/mand";
                if (url.Contains("barn"))
                    return "/barn";
                if (url.Contains("kvinde"))
                    return "/nyeste-toej/kvinde";
                if (url.Contains("diverse"))
                    return "/diverse/6-Diverse";
                
                return "/";
            }
            
            return "";
        }

     
        private ShowViewModel FillNavigation(ShowViewModel viewModel)
        {
            if ((Area)viewModel.Product.Area == Utilities.Area.Diverse)
            {
                var cats = FindCategoriesAndMakes((Area)viewModel.Product.Area, 0, 0);
                ViewBag.Categories = cats.CategoryResult;
            }
            else
            {
                ViewBag.Categories = FindCategoriesAndMakes((Area)viewModel.Product.Area, viewModel.Product.MainCategoryId, 0).CategoryResult;
                viewModel.Makes = FindCategoriesAndMakes((Area)viewModel.Product.Area, 0, viewModel.Product.CategoryId).MakeResult;
            }
            
            var titlestr = viewModel.Product.Title + " - " + viewModel.Product.CategoryName + " - Tøj";
            var metaDesc = Utilities.StripHtml(viewModel.Product.Description) + " - " + viewModel.Product.Title + " - " + viewModel.Product.CategoryName + " - Tøj";
            switch ((Area)viewModel.Product.Area)
            {
                case Utilities.Area.Men:
                    titlestr += " til mænd";
                    metaDesc += " til mænd";
                    break;
                case Utilities.Area.Women:
                    titlestr += " til kvinder";
                    metaDesc += " til kvinder";
                    break;
                case Utilities.Area.Kids:
                    titlestr += " til børn";
                    metaDesc += " til børn";
                    break;

            }
            ViewBag.MetaKeyWords = titlestr;
            titlestr += " (" + viewModel.Product.Id + ")";
            metaDesc = metaDesc + " (" + viewModel.Product.Id + ")";
            ViewBag.MetaDescription = metaDesc.LimitToLengthWithDots(160, false);
            if (metaDesc.Length < 80)
            {
                foreach (var prod in viewModel.Products.Skip(1))
                {
                    if (prod.Description != metaDesc && metaDesc.Length < 80)
                        metaDesc = metaDesc + " " + prod.Description;
                }
            }

            ViewBag.TheTitle = titlestr;
           
            ViewBag.PictureUrl = GetUrlHelper.GetPictureUrl(viewModel.Product.CompanyId, viewModel.Product.Picture, viewModel.Product.Id, viewModel.Product.Title);
            var forSite = "til mænd";
            if ((Area)viewModel.Product.Area == Utilities.Area.Women)
                forSite = "til kvinder";
            else if ((Area)viewModel.Product.Area == Utilities.Area.Kids)
                forSite = "til børn";
            else if ((Area)viewModel.Product.Area == Utilities.Area.Diverse || (Area)viewModel.Product.Area == Utilities.Area.All)
                forSite = "til folk i alle aldre";
            ViewBag.ForSite = forSite;
            ViewBag.TheArea = (Area)viewModel.Product.Area;
            ViewBag.MenuArea = (Area)viewModel.Product.Area;
            ViewBag.Controller = viewModel.Controller;
            ViewBag.MainCategoryName = _mainCategories[viewModel.Product.MainCategoryId];
            ViewBag.CategoryName = viewModel.Product.CategoryName;
            ViewBag.CurrentMainCategory = viewModel.Product.MainCategoryId;

            if (IsMobile())
                ViewBag.Make = viewModel.Product.Make.LimitToLengthWithDots(13);
            else
                ViewBag.Make = viewModel.Product.Make;
            return viewModel;
        }
        

        private CategorySearchQuery FindCategoriesAndMakes(Area theArea, int mainCategoryId, int categoryId)
        {
            var query = new CategorySearchQuery();
            query.TheArea = theArea;
            query.CategoryId = categoryId;
            query.MainCategoryId = mainCategoryId;
            query.SearchString = "";
            query.IndexPath = Server.MapPath("~/Indexer/CategoryIndex");
            return Category.SearchLucene(query);
        }

        private ProductSearchQuery DoSearch(FedtFetchProductResult product)
        {
            var query = new ProductSearchQuery();
            query.TheArea = (Area)product.Area;
            query.PageNumber = 1;
            query.MakeId = 0;
            query.MainCategoryId = 0;
            query.HitsPerPage = 13;
            query.MaxHits = 13;
            query.CategoryId = product.CategoryId;
            query.SearchString = product.Title;
            query.IndexPath = Server.MapPath("~/Indexer/LuceneIndex");
            query.SpecificSearch = false;
            return Product.SearchLucene(query);
        }

       

    }
}
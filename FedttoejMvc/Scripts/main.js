﻿window.___gcfg = { lang: 'da' };


addEventListener("keypress", function(event) {
    if (event.which == 13) {
        DoSearch('', '');
    }
});

//if (window.matchMedia("(max-width: 1000px)").matches) {
//    var element = document.getElementById("makes-menu");
//    var element2 = document.getElementById("makes-menu-complete");
//    if (element)
//        element.classList.add("dropdown-menu");
//    if (element2)
//        element2.classList.add("dropdown-menu");
//}
   

window.onresize = function () {
    if (window.matchMedia("(max-width: 1000px)").matches) {
        var element = document.getElementById("makes-menu");
        var element2 = document.getElementById("makes-menu-complete");
        if (element)
            element.classList.add("dropdown-menu");
        if (element2)
            element2.classList.add("dropdown-menu");
    } else {
        var element3 = document.getElementById("makes-menu");
        if (element3 != null)
            element3.className = "makes-menu";
        var element4 = document.getElementById("makes-menu-complete");
        if (element4 != null)
            element4.className = "makes-menu-complete";
    }
      
};

function DoSearch(area, areaText) {
    var search = document.getElementById('TxtSearch' + area).value;
    if (!search || search.length < 1)
        search = document.getElementById('TxtSearch2').value;
    document.location.href = '/soeg?SearchString=' + search + areaText;
    return false;
}

function articleView() {
    document.getElementById('article').className ='article-text';
    document.getElementById('articleViewLink').style.display = 'none';
}


var womenMenuOpen = false;
function openWomenMenu() {
    if (document.getElementById('men-menu'))
        document.getElementById('men-menu').style.display = 'none';
    if (document.getElementById('kids-menu'))
        document.getElementById('kids-menu').style.display = 'none';
    if (document.getElementById('makes-menu'))
        document.getElementById('makes-menu').style.display = 'none';
    if (document.getElementById('makes-menu-complete'))
        document.getElementById('makes-menu-complete').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';

    if (womenMenuOpen)
        document.getElementById('women-menu').style.display = 'none';
    else
        document.getElementById('women-menu').style.display = 'block';
    womenMenuOpen = !womenMenuOpen;
    menMenuOpen = false;
    kidsMenuOpen = false;
    diverseMenuOpen = false;

    document.getElementById('TxtSearch-women').focus();
}

var menMenuOpen = false;
function openMenMenu() {
    if (document.getElementById('women-menu'))
        document.getElementById('women-menu').style.display = 'none';
    if (document.getElementById('kids-menu'))
        document.getElementById('kids-menu').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';
    if (document.getElementById('makes-menu'))
        document.getElementById('makes-menu').style.display = 'none';
    if (document.getElementById('makes-menu-complete'))
        document.getElementById('makes-menu-complete').style.display = 'none';

    if (menMenuOpen)
        document.getElementById('men-menu').style.display = 'none';
    else
        document.getElementById('men-menu').style.display = 'block';
    menMenuOpen = !menMenuOpen;
    womenMenuOpen = false;
    kidsMenuOpen = false;
    diverseMenuOpen = false;

    document.getElementById('TxtSearch-men').focus();
}

var kidsMenuOpen = false;
function openKidsMenu() {
    if (document.getElementById('women-menu'))
        document.getElementById('women-menu').style.display = 'none';
    if (document.getElementById('men-menu'))
        document.getElementById('men-menu').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';
    if (document.getElementById('makes-menu'))
        document.getElementById('makes-menu').style.display = 'none';
    if (document.getElementById('makes-menu-complete'))
        document.getElementById('makes-menu-complete').style.display = 'none';

    if (kidsMenuOpen)
        document.getElementById('kids-menu').style.display = 'none';
    else
        document.getElementById('kids-menu').style.display = 'block';
    kidsMenuOpen = !kidsMenuOpen;
    womenMenuOpen = false;
    menMenuOpen = false;
    diverseMenuOpen = false;

    document.getElementById('TxtSearch-kids').focus();
}

var diverseMenuOpen = false;
function openDiverseMenu() {
    if (document.getElementById('women-menu'))
        document.getElementById('women-menu').style.display = 'none';
    if (document.getElementById('men-menu'))
        document.getElementById('men-menu').style.display = 'none';
    if (document.getElementById('kids-menu'))
        document.getElementById('kids-menu').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';
    if (document.getElementById('makes-menu'))
        document.getElementById('makes-menu').style.display = 'none';
    if (document.getElementById('makes-menu-complete'))
        document.getElementById('makes-menu-complete').style.display = 'none';

    if (diverseMenuOpen)
        document.getElementById('diverse-menu').style.display = 'none';
    else
        document.getElementById('diverse-menu').style.display = 'block';
    diverseMenuOpen = !diverseMenuOpen;
    womenMenuOpen = false;
    menMenuOpen = false;
    kidsMenuOpen = false;

    document.getElementById('TxtSearch-div').focus();
}



var subcategorymenuOpen = false;
if (document.getElementById("sub-cat-header")) {
    document.getElementById("sub-cat-header").addEventListener("click", function () {
        if (subcategorymenuOpen)
            document.getElementById('subcategory-menu').style.display = 'none';
        else
            document.getElementById('subcategory-menu').style.display = 'block';
        subcategorymenuOpen = !subcategorymenuOpen;
       
        if (document.getElementById('main-menu'))
            document.getElementById('main-menu').style.display = 'none';
        if (document.getElementById('category-menu'))
            document.getElementById('category-menu').style.display = 'none';
        if (document.getElementById('makes-menu'))
            document.getElementById('makes-menu').style.display = 'none';
        if (document.getElementById('makes-menu-complete'))
            document.getElementById('makes-menu-complete').style.display = 'none';
    });
}


var makemenuOpen = false;
function openMakeMenu() {
    if (makemenuOpen) {
        document.getElementById('makes-menu').style.display = 'none';
        document.getElementById('makes-menu-complete').style.display = 'none';
    } else
        document.getElementById('makes-menu').style.display = 'block';
    makemenuOpen = !makemenuOpen;

    if (document.getElementById('main-menu'))
        document.getElementById('main-menu').style.display = 'none';
    if (document.getElementById('category-menu'))
        document.getElementById('category-menu').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';
}

//function closeCatfish() {
//    $("#catfish").css("display", "none");
//    setCookie("CatfishClosed", "1", 1);
//}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

var mobileSearchVisible = false;
function toggleMobileSearch() {
    if (document.getElementById('women-menu'))
        document.getElementById('women-menu').style.display = 'none';
    if (document.getElementById('men-menu'))
        document.getElementById('men-menu').style.display = 'none';
    if (document.getElementById('kids-menu'))
        document.getElementById('kids-menu').style.display = 'none';
    if (document.getElementById('subcategory-menu'))
        document.getElementById('subcategory-menu').style.display = 'none';
    if (document.getElementById('makes-menu'))
        document.getElementById('makes-menu').style.display = 'none';
    if (document.getElementById('makes-menu-complete'))
        document.getElementById('makes-menu-complete').style.display = 'none';

    if (mobileSearchVisible) {
        document.getElementById('mobile-search').style.display = 'none';
        mobileSearchVisible = false;
    } else {
        document.getElementById('mobile-search').style.display = 'block';
        mobileSearchVisible = true;
        document.getElementById('TxtSearch').focus();
    }

}

function showMakes() {

    document.getElementById('makes-menu').innerHTML = document.getElementById('makes-menu-complete').innerHTML;
    document.getElementById('makes-menu-complete').innerHTML = '';
    //document.getElementById('makes-menu').style.display = 'none';
    //document.getElementById('makes-menu-complete').style.display = 'block';
}

function readMore() {
    document.getElementById("article").style.height = "auto";
    document.getElementById("readless").style.display = "";
    document.getElementById("readmore").style.display = "none";
}

function readLess() {
    document.getElementById("article").style.height = "100px";
    document.getElementById("readless").style.display = "none";
    document.getElementById("readmore").style.display = "";
}


// Set the price gap
let priceGap = 10;

// Adding event listners to price input elements
if (typeof priceInputvalue !== 'undefined') {
    for (let i = 0; i < priceInputvalue.length; i++) {
        priceInputvalue[i].addEventListener("input", e => {

            // Parse min and max values of the range input
            let minp = parseInt(priceInputvalue[0].value);
            let maxp = parseInt(priceInputvalue[1].value);
            if (minp < 0) {
                alert("minimum kan ikke være under 0 kr.");
                priceInputvalue[0].value = 0;
                minp = 0;
            }

            // Validate the input values
            if (maxp > 100000) {
                alert("Maksimum kan ikke være over 100.000 kr.");
                priceInputvalue[1].value = 100000;
                maxp = 100000;
            }
              
        });

        priceInputvalue[i].addEventListener("focus", () => priceInputvalue[i].select());
    }
}
function searchPrice() {
    var query = "";

    if (priceInputvalue[0].value != "0" || priceInputvalue[1].value != "10000") {
        query = "?priceFrom=" + priceInputvalue[0].value + "&priceTo=" + priceInputvalue[1].value;
    }

    var sorting = document.getElementById("sorting");

    if (sorting.value != "0") {
        if (query != "")
            query += "&";
        else
            query += "?";

        query += "sorting=" + sorting.value;
    }

    location.href = currentUrl + query;
}

function togglePriceMenu() {
    if (priceMenuVisible) {
        document.getElementById("mobile-price-search").style.display = "none";
    }
    else {
        document.getElementById("mobile-price-search").style.display = "block";
    }
    priceMenuVisible = !priceMenuVisible;
}

function toggleSubmenu(area, categoryId) {
    var menu = "submenu-" + area + "-" + categoryId;
    console.log(menu);
    document.getElementById(menu).classList.toggle('hidden');
}

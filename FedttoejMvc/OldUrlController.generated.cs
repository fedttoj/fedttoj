// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
#pragma warning disable 1591, 3008, 3009
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using T4MVC;
namespace FedttoejMvc.Controllers
{
    public partial class OldUrlController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public OldUrlController() { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected OldUrlController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult MainCategoryWithMakeOld()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MainCategoryWithMakeOld);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult CategoryWithMakeOld()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CategoryWithMakeOld);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public OldUrlController Actions { get { return MVC.OldUrl; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "OldUrl";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "OldUrl";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string MainCategoryWithMakeOld = "MainCategoryWithMakeOld";
            public readonly string CategoryWithMakeOld = "CategoryWithMakeOld";
            public readonly string SitemapOld = "SitemapOld";
            public readonly string AboutOld = "AboutOld";
            public readonly string ContactOld = "ContactOld";
            public readonly string SearchResultOld = "SearchResultOld";
            public readonly string RedirectErrors = "RedirectErrors";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string MainCategoryWithMakeOld = "MainCategoryWithMakeOld";
            public const string CategoryWithMakeOld = "CategoryWithMakeOld";
            public const string SitemapOld = "SitemapOld";
            public const string AboutOld = "AboutOld";
            public const string ContactOld = "ContactOld";
            public const string SearchResultOld = "SearchResultOld";
            public const string RedirectErrors = "RedirectErrors";
        }


        static readonly ActionParamsClass_MainCategoryWithMakeOld s_params_MainCategoryWithMakeOld = new ActionParamsClass_MainCategoryWithMakeOld();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_MainCategoryWithMakeOld MainCategoryWithMakeOldParams { get { return s_params_MainCategoryWithMakeOld; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_MainCategoryWithMakeOld
        {
            public readonly string gender = "gender";
            public readonly string cat = "cat";
            public readonly string mainId = "mainId";
            public readonly string categoryId = "categoryId";
            public readonly string make = "make";
        }
        static readonly ActionParamsClass_CategoryWithMakeOld s_params_CategoryWithMakeOld = new ActionParamsClass_CategoryWithMakeOld();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_CategoryWithMakeOld CategoryWithMakeOldParams { get { return s_params_CategoryWithMakeOld; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_CategoryWithMakeOld
        {
            public readonly string gender = "gender";
            public readonly string cat = "cat";
            public readonly string subcat = "subcat";
            public readonly string categoryIdAndmake = "categoryIdAndmake";
            public readonly string categoryId = "categoryId";
            public readonly string make = "make";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
            }
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_OldUrlController : FedttoejMvc.Controllers.OldUrlController
    {
        public T4MVC_OldUrlController() : base(Dummy.Instance) { }

        [NonAction]
        partial void MainCategoryWithMakeOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string gender, string cat, int mainId, int categoryId, string make);

        [NonAction]
        public override System.Web.Mvc.ActionResult MainCategoryWithMakeOld(string gender, string cat, int mainId, int categoryId, string make)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MainCategoryWithMakeOld);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "gender", gender);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "cat", cat);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "mainId", mainId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "categoryId", categoryId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "make", make);
            MainCategoryWithMakeOldOverride(callInfo, gender, cat, mainId, categoryId, make);
            return callInfo;
        }

        [NonAction]
        partial void CategoryWithMakeOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string gender, string cat, string subcat, string categoryIdAndmake);

        [NonAction]
        public override System.Web.Mvc.ActionResult CategoryWithMakeOld(string gender, string cat, string subcat, string categoryIdAndmake)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CategoryWithMakeOld);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "gender", gender);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "cat", cat);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "subcat", subcat);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "categoryIdAndmake", categoryIdAndmake);
            CategoryWithMakeOldOverride(callInfo, gender, cat, subcat, categoryIdAndmake);
            return callInfo;
        }

        [NonAction]
        partial void CategoryWithMakeOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string gender, string cat, string subcat, int categoryId, string make);

        [NonAction]
        public override System.Web.Mvc.ActionResult CategoryWithMakeOld(string gender, string cat, string subcat, int categoryId, string make)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.CategoryWithMakeOld);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "gender", gender);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "cat", cat);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "subcat", subcat);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "categoryId", categoryId);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "make", make);
            CategoryWithMakeOldOverride(callInfo, gender, cat, subcat, categoryId, make);
            return callInfo;
        }

        [NonAction]
        partial void SitemapOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult SitemapOld()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SitemapOld);
            SitemapOldOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void AboutOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult AboutOld()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.AboutOld);
            AboutOldOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void ContactOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult ContactOld()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.ContactOld);
            ContactOldOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void SearchResultOldOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult SearchResultOld()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SearchResultOld);
            SearchResultOldOverride(callInfo);
            return callInfo;
        }

        [NonAction]
        partial void RedirectErrorsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo);

        [NonAction]
        public override System.Web.Mvc.ActionResult RedirectErrors()
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.RedirectErrors);
            RedirectErrorsOverride(callInfo);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009

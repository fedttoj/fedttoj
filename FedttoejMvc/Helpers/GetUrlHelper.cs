﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using FedttoejMvc.Models;
using FedttoejMvc.Models.Data;
using FedttoejMvc.Models.ViewModels;
using static FedttoejMvc.Models.Utilities;

namespace FedttoejMvc.Helpers
{
    public static class GetUrlHelper
    {
        private static readonly Dictionary<int, string> _mainCategories = Utilities.MainCategories();

        public static string GetCategoryUrl(Controller contoller, Utilities.Area area, string mainCategoryName, int categoryId, string categoryName)
        {
            return contoller.Url.Action(MVC.Home.SubCategory(Utilities.GetAreaString(area), Utilities.Urlencode(mainCategoryName), categoryId + "-" + Utilities.Urlencode(categoryName)));
        }

        public static string GetMainCategoryUrl(Controller controller, Utilities.Area area, int mainCategoryId, string mainCategoryName)
        {
            return controller.Url.Action(MVC.Home.MainCategory(Utilities.GetAreaString(area), mainCategoryId + "-" + Utilities.Urlencode(mainCategoryName)));
        }

        public static string GetNewestUrl(Controller controller, Utilities.Area area)
        {
            return controller.Url.Action(MVC.Home.Newest(Utilities.GetAreaString(area)));
        }

        public static string GetReducedUrl(Controller controller, Utilities.Area area)
        {
            return controller.Url.Action(MVC.Home.ReducedPrices(Utilities.GetAreaString(area)));
        }

        public static string GetMainCategoryMakeUrl(Controller contoller, Utilities.Area area, int mainCategoryId, int categoryId, string categoryName, int makeId, string make)
        {
            if (make == "")
                make = "ingen mærke";
            return contoller.Url.Action(MVC.Home.MainCategoryWithMake(Utilities.GetAreaString(area), Utilities.Urlencode(_mainCategories[mainCategoryId]), mainCategoryId, categoryId, makeId, Utilities.Urlencode(make)));
        }

        public static string GetCategoryMakeUrl(Controller contoller, Utilities.Area area, int mainCategoryId, int categoryId, string categoryName, int makeId, string make)
        {
            if (make == "")
                make = "ingen mærke";
            return contoller.Url.Action(MVC.Home.CategoryWithMake(Utilities.GetAreaString(area), Utilities.Urlencode(_mainCategories[mainCategoryId]), Utilities.Urlencode(categoryName), categoryId, makeId, Utilities.Urlencode(make)));
            
        }

        public static string GetAnswerUrl(Controller contoller, int id, string name)
        {
            return contoller.Url.Action(MVC.Home.Answers(id + "-" + Utilities.Urlencode(name.Replace("?", ""))));
        }
        
        public static string GetProductUrl(Controller controller, Product product)
        {
            return
                controller.Url.Action(MVC.Show.Show(Utilities.GetAreaString(product.TheArea),
                    Utilities.Urlencode(_mainCategories[product.MainCategoryId]), Utilities.Urlencode(product.CategoryName),
                    product.Id + "-" + Utilities.Urlencode(product.Title.Replace(":", "--").Replace("/", "").Replace("%2f", ""))));

        }

        public static string GetProductUrl(Controller controller, FedtFetchProductResult product)
        {
            return
                controller.Url.Action(MVC.Show.Show(Utilities.GetAreaString((Area)product.Area),
                    Utilities.Urlencode(_mainCategories[product.MainCategoryId]), Utilities.Urlencode(product.CategoryName),
                    product.Id + "-" + Utilities.Urlencode(product.Title.Replace(":", "--").Replace("/", "").Replace("%2f", ""))));

        }

        public static string GetProductUrl(Controller controller, GetBestReducedPricesPercentResult product)
        {
            return
                controller.Url.Action(MVC.Show.Show(Utilities.GetAreaString((Area)product.Area),
                    Utilities.Urlencode(_mainCategories[product.MainCategoryId]), Utilities.Urlencode(product.CategoryName),
                    product.Id + "-" + Utilities.Urlencode(product.Title.Replace(":", "--").Replace("/", "").Replace("%2f", ""))));

        }

        public static string GetProductUrl(Controller controller, GetBestReducedPricesKrResult product)
        {
            return
                controller.Url.Action(MVC.Show.Show(Utilities.GetAreaString((Area)product.Area),
                    Utilities.Urlencode(_mainCategories[product.MainCategoryId]), Utilities.Urlencode(product.CategoryName),
                    product.Id + "-" + Utilities.Urlencode(product.Title.Replace(":", "--").Replace("/", "").Replace("%2f", ""))));

        }

        public static string GetPictureUrl(FedtFetchProductResult product)
        {
            return GetPictureUrl(product.CompanyId, product.Picture, product.Id, product.Title);
        }

        public static string GetPictureUrl(Product product)
        {
            return GetPictureUrl(product.CompanyId, product.Picture, product.Id, product.Title);
        }

        public static string GetArticleUrl(ArticleViewModel article)
        {
            return "/artikler/" + article.Url;
        }

        public static string GetVideoUrl(VideoViewModel video)
        {
            return "/videos/" + video.Url;
        }

        public static string GetPictureUrl(int companyId, string pricture, int productId, string title)
        {
            if (companyId == 37 || companyId == 39 || companyId == 48) //Stylepit
                return pricture;
            else if (companyId == 27)
                return pricture.Replace("http://", "https://");
            return GetPictureDomain(productId) + "/billeder/single/" + productId + "-" + Utilities.Urlencode(title.Replace(":", "--").Replace("/", "").Replace("%2f", ""));
        }

        private static string GetPictureDomain(int id)
        {
            var url = "https://billeder1.fedttoj.dk";
            if (id % 3 > 0)
                url = "https://billeder2.fedttoj.dk";
            if (id % 3 > 1)
                url = "https://billeder3.fedttoj.dk";
            return url;
        }
    }
}
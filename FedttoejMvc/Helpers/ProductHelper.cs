﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace FedttoejMvc.Helpers
{
    public static class ProductHelper
    {
        private static int _maxPages = 9;
        private static int _halfPages = 5;
        private static string _previous = "&laquo; Forrige";
        private static string _next = "Næste &raquo;";

        public  static Dictionary<string, int> SetPaging(int numberOfProducts, int pageSize, int pageNumber, bool isMobile)
        {
            if (isMobile)
            {
                _maxPages = 5;
                _halfPages = 3;
                _previous = "&laquo;";
                _next = "&raquo;";
            }
            else
            {
                _maxPages = 9;
                _halfPages = 5;
                _previous = "&laquo; Forrige";
                _next = "Næste &raquo;";
            }

            var numberOfPages = numberOfProducts / pageSize;
            if (numberOfProducts % pageSize != 0)
                numberOfPages++;

            if (numberOfPages < 2)
                return new Dictionary<string, int>();
            var startpage = pageNumber - _halfPages;
            if (startpage < 1)
                startpage = 1;

            var endPage = pageNumber + _halfPages-1;
            if (endPage - startpage < _maxPages)
                endPage = startpage + _maxPages;
            if (endPage > numberOfPages)
                endPage = numberOfPages;
            if (endPage - startpage < _maxPages)
                startpage = endPage - _maxPages;
            if (startpage < 1)
                startpage = 1;

            var paging = new Dictionary<string, int>();
            if (pageNumber > 1)
                paging.Add(_previous, pageNumber - 1);

            for (var i = startpage; i <= endPage; i++)
            {
                if (i == pageNumber)
                    paging.Add(i.ToString(CultureInfo.InvariantCulture), 0);
                else
                    paging.Add(i.ToString(CultureInfo.InvariantCulture), i);
            }
            if (numberOfProducts > pageSize * pageNumber)
                paging.Add(_next, pageNumber + 1);

            return paging;
        }


    }
}